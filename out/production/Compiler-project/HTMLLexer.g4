
lexer grammar  HTMLLexer;

HTML_COMMENT
    : '<!--' .*? '-->'
    ;
//operation / logic / String / Test Samples
HTML_CONDITIONAL_COMMENT
    : '<![' .*? ']>'
    ;

XML
    : '<?xml' .*? '>'
    ;

CDATA
    : '<![CDATA[' .*? ']]>'
    ;

DTD
    : '<!' .*? '>'
    ;

SCRIPTLET
    : '<?' .*? '?>'
    | '<%' .*? '%>'
    ;

SEA_WS
    :  (' '|'\t'|'\r'? '\n')+
    ;

SCRIPT_OPEN
    : '<script' .*? '>' ->pushMode(SCRIPT)
    ;

STYLE_OPEN
    : '<style' .*? '>'  ->pushMode(STYLE)
    ;

TAG_OPEN
    : '<' -> pushMode(TAG)
    ;


HTML_TEXT
    : ~[<{]+
    ;

// lexing mode for attribute values

// tag declarations



BINDING_OPEN
    : ' '* '{{' ' '* -> pushMode(QUOTETION_MODE)
    ;




/*
mode BINDING_MODE;

BINDING_CLOSE
    : ' '* '}}' ' '* -> popMode
    ;

PIPE: ' '* '|' ' '*;

VARNAME_b: VARNAMEE;
*/

mode TAG;

CP_SHOW
    : 'cp-show' ' '*  -> pushMode(CP_ATTVALUE)
    ;

CP_FOR
    : 'cp-for' ' '* -> pushMode(CP_ATTVALUE)
    ;

CP_HIDE
    : 'cp-hide' ' '* -> pushMode(CP_ATTVALUE)
    ;

CP_IF
    : 'cp-if' ' '*  -> pushMode(CP_ATTVALUE)
    ;

CP_SWITCH
    :'cp-switch' ' '* -> pushMode(CP_ATTVALUE)
    ;

CP_SWITCH_CASE
    : 'cp-switch-case' ' '* -> pushMode(CP_ATTVALUE)
    ;

CP_SWITCHDEFAULT
    : 'cp-switchDefault' ;

EVENT_NAME
    : '@' [a-zA-Z_][a-zA-Z0-9_]* -> pushMode(CP_ATTVALUE)
    ;


TAG_EQUALS
    : '=' -> pushMode(ATTVALUE)
    ;

TAG_NAME
    : TAG_NameStartChar TAG_NameChar*
    ;

TAG_WHITESPACE
    : [ \t\r\n] -> channel(HIDDEN)
    ;

TAG_CLOSE
    : '>' -> popMode
    ;

TAG_SLASH_CLOSE
    : '/>' -> popMode
    ;

TAG_SLASH
    : '/'
    ;

fragment
HEXDIGIT
    : [a-fA-F0-9]
    ;

fragment
DIGIT
    : [0-9]
    ;

fragment
TAG_NameChar
    : TAG_NameStartChar
    | '-'
    | '_'
    | '.'
    | DIGIT
    | '\u00B7'
    | '\u0300'..'\u036F'
    | '\u203F'..'\u2040'
    ;

fragment
TAG_NameStartChar
    : [:a-zA-Z]
    | '\u2070'..'\u218F'
    | '\u2C00'..'\u2FEF'
    | '\u3001'..'\uD7FF'
    | '\uF900'..'\uFDCF'
    | '\uFDF0'..'\uFFFD'
    ;




// <scripts>

mode SCRIPT;

SCRIPT_BODY
    : .*? '</script>' -> popMode
    ;

SCRIPT_SHORT_BODY
    : .*? '</>' -> popMode
    ;


// <styles>

mode STYLE;

STYLE_BODY
    : .*? '</style>' -> popMode
    ;

STYLE_SHORT_BODY
    : .*? '</>' -> popMode
    ;


// attribute values

mode ATTVALUE;

// an attribute value may have spaces b/t the '=' and the value
ATTVALUE_VALUE
    : ' '* ATTRIBUTE -> popMode
    ;

ATTRIBUTE
    : DOUBLE_QUOTE_STRING
    | SINGLE_QUOTE_STRING
    | ATTCHARS
    | HEXCHARS
    | DECCHARS
    ;

fragment ATTCHARS
    : ATTCHAR+ ' '?
    ;

fragment ATTCHAR
    : '-'
    | '_'
    | '.'
    | '/'
    | '+'
    | ','
    | '?'
    | '='
    | ':'
    | ';'
    | '#'
    | [0-9a-zA-Z]
    ;

fragment HEXCHARS
    : '#' [0-9a-fA-F]+
    ;

fragment DECCHARS
    : [0-9]+ '%'?
    ;

fragment DOUBLE_QUOTE_STRING
    : '"' ~[<"]* '"'
    ;

fragment SINGLE_QUOTE_STRING
    : '\'' ~[<']* '\''
    ;
fragment SEMICOLON
    : ';'
    ;


fragment VARNAMEE: ' '* [a-zA-Z_] [a-zA-Z0-9_]* ' '*;

mode CP_ATTVALUE;

EQUALL///
    : ' '* '=' ' '*;///

QUOTETION_OPEN
    : ' '* '"' ' '* -> pushMode(QUOTETION_MODE)
    ;



mode QUOTETION_MODE;




    QUOTETION_CLOSE
        : ' '* '"' ' '* -> popMode , popMode
        ;



BINDING_CLOSE
        : ' '* '}}' ' '* -> mode(DEFAULT_MODE)
        ;


    /* Constants */


    NUMBER:  ' '* DIGIT+ ' '* ;

    CHAR: ' '* '\'' ' '* ~[<'"]* ' '* '\'' ' '*;



    /* Others */
    QMARK: ' '* '?' ' '*;

    COLUMN: ' '* ':' ' '*;

    COLUMN_B: ' '* ':' ' '*  '"' ' '* ~[<"]* ' '* '"' ' '*;

    DOT: '.';

    NOT: ' '* '!' ' '*;

    PIPE: ' '* '|' ' '*;

    IN: ' '* 'in' ' '*;

    INDEX: ' '* 'index' ' '*;

    BOOLEAN: ' '* '>'| '<' | '<=' | '>=' | '==' | '!=' ' '*;

    LOGIC: ' '* '&&' | '||' ' '*;

    PLUSPLUS : ' '* '++' ' '*;

    PLUS: ' '* '+' ' '*;

    MINUS: ' '* '-' ' '*;

    MINUSMINUS : ' '* '--' ' '*;

    MULT: ' '* '*' ' '*;

    DIV: ' '* '/' ' '*;

    REMAIN: ' '* '%' ' '*;

    ARRAYOPEN: '[' ' '* ;

    ARRAYCLOSE: ' '* ']' ' '* ;

    BRACOPEN: ' '* '(' ' '* ;

    BRACCLOSE: ' '* ')' ' '* ;
//    PLUS : ' '* '+' ' '*;

    SEMICOLONN:  ' '* SEMICOLON ' '*;

    EQUAL: ' '* '=' ' '*;

    COMMA: ' '* ',' ' '*;

    TRUE: ' '* 'true' ' '*;

    FALSE: ' '* 'false' ' '*;

    DQUTION: ' '*  '"' ' '*;

    CURLYOPEN: ' '* '{' ' '*;

    CURLYCLOSE: ' '* '}' ' '*;

    APOSTROPHE: ' '* '\'' ' '*;



    /* Variables */
   VARNAME:VARNAMEE;

//    STRING: ' '* '"' ' '* ~[<"]* ' '* '"' ' '*;


