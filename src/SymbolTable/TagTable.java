package SymbolTable;

import java.util.ArrayList;

public class TagTable {
    ArrayList<TagInfo> tagTable = new ArrayList<>();

    public ArrayList<TagInfo> getTagTable() {
        return tagTable;
    }

    public void setTagTable(ArrayList<TagInfo> tagTable) {
        this.tagTable = tagTable;
    }

    public void print() {
        for (TagInfo tag : tagTable) {
            System.out.println(tag);
            System.out.println();
        }
    }
}
