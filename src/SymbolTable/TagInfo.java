package SymbolTable;

//import ast.model.Attribue.ElementAttribute;
//import java.util.ArrayList;

public class TagInfo {
    String tagName;
    Scope tagScope;
    //ArrayList<ElementAttribute> tagAttribute = new ArrayList<>();
    int scopeCreated;
    boolean isClose;

    public TagInfo(String tagName, Scope tagScope, int scopeCreated, boolean isClose) {
        this.tagName = tagName;
        this.tagScope = tagScope;
        this.scopeCreated = scopeCreated;
        this.isClose = isClose;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Scope getTagScope() {
        return tagScope;
    }

    public void setTagScope(Scope tagScope) {
        this.tagScope = tagScope;
    }

    public int getScopeCreated() {
        return scopeCreated;
    }

    public void setScopeCreated(int scopeCreated) {
        this.scopeCreated = scopeCreated;
    }

    public boolean isClose() {
        return isClose;
    }

    public void setClose(boolean close) {
        isClose = close;
    }

    @Override
    public String toString() {
        return "TagInfo{" +
                "tagName='" + tagName + '\'' +
                ", tagScope=" + tagScope +
                ", scopeCreated=" + scopeCreated +
                ", isClose=" + isClose +
                '}';
    }
}
