package SymbolTable;

import java.util.LinkedHashMap;
import java.util.Map;

public class Scope {

    private static int contID = 0;
    private int id;
    private Scope parent;
    private Map<String, Symbol> symbolMap = new LinkedHashMap<>();


    public Scope(Scope parent) {
        this.parent = parent;
        this.id = ++contID;
    }

    public void addSymbol(String name, Symbol symbol) {
        this.symbolMap.put(name, symbol);
    }

    public static int getContID() {
        return contID;
    }

    public static void setContID(int contID) {
        Scope.contID = contID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Scope getParent() {
        return parent;
    }

    public void setParent(Scope parent) {
        this.parent = parent;
    }

    public Map<String, Symbol> getSymbolMap() {
        return symbolMap;
    }

    public void setSymbolMap(Map<String, Symbol> symbolMap) {
        this.symbolMap = symbolMap;
    }

    public Scope searchForSymbol(String symbol) {
        /*if(symbolMap.get(symbol)!=null){
            return this;
        }*/
        for (String name : symbolMap.keySet()) {
            Symbol temp = symbolMap.get(name);
            System.out.println(temp.getName() + symbol + "+++++++++++++++++++++++++++++++++++++" + symbol);
            if (temp.getName() == symbol)
                return this;
        }
        return null;
    }
}
