package ast.visitor;

import ast.model.*;
import ast.model.Array.*;
import ast.model.Attribue.AttributeValue;
import ast.model.Attribue.CpAttribute;
import ast.model.Attribue.ElementAttribute;
import ast.model.Binding.Binding;
import ast.model.Binding.BindingPipe;
import ast.model.Binding.Binding_exp;
import ast.model.Binding.Expr_Bin;
import ast.model.Event.Event;
import ast.model.Expression.Expression;
import ast.model.Expression.One_line_exp;
import ast.model.For.*;
import ast.model.Function.AnotherFunctionParameter;
import ast.model.Function.Function;
import ast.model.Function.FunctionParameter;
import ast.model.Function.Function_expr;
import ast.model.Hide.Cp_hide;
import ast.model.HtmlS.*;
import ast.model.If.Cp_if;
import ast.model.If.One_line_condition;
import ast.model.Object.AnotherObj;
import ast.model.Object.Expr_object;
import ast.model.Object.Objectt;
import ast.model.Operation.*;
import ast.model.Show.Cp_show;
import ast.model.Switch.Cp_switch;
import ast.model.Switch.Cp_switchDefault;
import ast.model.Switch.Cp_switch_case;
import ast.model.Var.*;
import ast.nodes.HTMLElements;
import ast.nodes.HtmlDocument;

public interface ASTVisitor {
    void visit(HTMLElements htmlElements);

    void visit(Array array);

    void visit(AnotherArrayElement anotherArrayElement);

    void visit(ArrayIndex arrayIndex);

    void visit(ArrayElementExp arrayElementExp);

    void visit(Array_element array_element);

    void visit(ElementAttribute elementAttribute);

    void visit(CpAttribute cpAttribute);

    void visit(AttributeValue attributeValue);

    void visit(Expr_Bin expr_bin);

    void visit(BindingPipe bindingPipe);

    void visit(Binding_exp binding_exp);

    void visit(Binding binding);

    void visit(Event event);

    void visit(Expression expression);

    void visit(One_line_exp one_line_exp);

    void visit(AfterIN afterIN);

    void visit(AfterSemiColonn afterSemiColonn);

    void visit(BeforIN beforIN);

    void visit(Cp_for cp_for);

    void visit(for_ExprFirst for_exprFirst);

    void visit(for_ExprSecond for_exprSecond);

    void visit(AnotherFunctionParameter anotherFunctionParameter);

    void visit(Function function);

    void visit(Function_expr function_expr);

    void visit(FunctionParameter functionParameter);

    void visit(Cp_hide cp_hide);

    void visit(ContainerHtmlContent containerHtmlContent);

    void visit(HtmlAttribute htmlAttribute);

    void visit(HtmlChardata htmlChardata);

    void visit(HtmlComment htmlComment);

    void visit(HtmlContent htmlContent);

    void visit(HtmlElement htmlElement);

    void visit(HtmlMisc htmlMisc);

    void visit(HtmlStructure htmlStructure);

    void visit(Script script);

    void visit(ScriptletOrSeaWs scriptletOrSeaWs);

    void visit(Style style);

    void visit(Cp_if cp_if);

    void visit(One_line_condition one_line_condition);

    void visit(AnotherObj anotherObj);

    void visit(Expr_object expr_object);

    void visit(Objectt objectt);
/*
    void visit(OperationDELETED operation);

    void visit(OperationLeftSideDELETED operationLeftSide);

    void visit(OperationRightSideDELETED operationRightSide);
*/
    void visit(Cp_show cp_show);

    void visit(Cp_switch cp_switch);

    void visit(Cp_switch_case cp_switch_case);

    void visit(Cp_switchDefault cp_switchDefault);

    void visit(Constt constt);

    void visit(DotVar dotVar);

    void visit(Simple_var simple_var);

    void visit(Var var);

    void visit(HtmlDocument htmlDocument);

    void visit (OperationElement operationElement);

    void visit (operationSideWithDivMultRemain operationSideWithDivMultRemain);

    void visit (OperationSide OperationSide);

    void visit (OperationSideWithPlusMinus operationSideWithPlusMinus);

    void visit (MathOperation mathOperation);

    void visit (booleanSide booleanSide);

    void visit (boolen boolen);

    void visit (logicSide logicSide);

    void visit (logic logic);

    void visit(ConstOperation constOperation);

    void visit(ConstSide constSide);
    //public void accept(ASTVisitor astVisitor);
}
