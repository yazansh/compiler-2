package ast.visitor;


import SymbolTable.Scope;
import SymbolTable.TagInfo;
import ast.model.Array.*;
import ast.model.Attribue.AttributeValue;
import ast.model.Attribue.CpAttribute;
import ast.model.Attribue.ElementAttribute;
import ast.model.Binding.Binding;
import ast.model.Binding.BindingPipe;
import ast.model.Binding.Binding_exp;
import ast.model.Binding.Expr_Bin;
import ast.model.Event.Event;
import ast.model.Expression.Expression;
import ast.model.Expression.One_line_exp;
import ast.model.For.*;
import ast.model.Function.AnotherFunctionParameter;
import ast.model.Function.Function;
import ast.model.Function.FunctionParameter;
import ast.model.Function.Function_expr;
import ast.model.Hide.Cp_hide;
import ast.model.HtmlS.*;
import ast.model.If.Cp_if;
import ast.model.If.One_line_condition;
import ast.model.Object.AnotherObj;
import ast.model.Object.Expr_object;
import ast.model.Object.Objectt;
import ast.model.Operation.*;
import ast.model.Show.Cp_show;
import ast.model.Switch.Cp_switch;
import ast.model.Switch.Cp_switchDefault;
import ast.model.Switch.Cp_switch_case;
import ast.model.Var.*;
import ast.model.booleanSide;
import ast.model.boolen;
import ast.nodes.HTMLElements;
import ast.nodes.HtmlDocument;
import gen.HTMLParser;
import gen.HTMLParserBaseVisitor;
import tareq.Main;


public class BaseVisitor extends HTMLParserBaseVisitor {

    @Override
    public HtmlDocument visitHtmlDocument(HTMLParser.HtmlDocumentContext ctx) {
        System.out.println("visitHtmlDocument");
        creatScope();
        HtmlDocument htmlDocument = new HtmlDocument();
        htmlDocument.setScopeID(getCurrentScope().getId());


        for (int i = 0; i < ctx.htmlElements().size(); i++) {
            htmlDocument.getElements().add(visitHtmlElements(ctx.htmlElements(i)));
        }
        for (int i = 0; i < ctx.scriptletOrSeaWs().size(); i++) {
            htmlDocument.getScriptletOrSeaWs().add(visitScriptletOrSeaWs(ctx.scriptletOrSeaWs(i)));
        }
        if (ctx.DTD() != null)
            htmlDocument.setDTD(ctx.DTD().toString());

        removeScope();
        return htmlDocument;
    }


    @Override
    public HTMLElements visitHtmlElements(HTMLParser.HtmlElementsContext ctx) {
        System.out.println("visitHtmlElements");
        HTMLElements htmlElements = new HTMLElements();
        htmlElements.setScopeID(getCurrentScope().getId());
        for (int i = 0; i < ctx.htmlMisc().size(); i++) {
            htmlElements.getHtmlMiscs().add(visitHtmlMisc(ctx.htmlMisc(i)));
        }
        htmlElements.setHtmlElement(visitHtmlElement(ctx.htmlElement()));
        return htmlElements;
    }

    @Override
    public ScriptletOrSeaWs visitScriptletOrSeaWs(HTMLParser.ScriptletOrSeaWsContext ctx) {
        System.out.println("visitScriptletOrSeaWs");
        ScriptletOrSeaWs scriptletOrSeaWs = new ScriptletOrSeaWs();
        scriptletOrSeaWs.setScopeID(getCurrentScope().getId());
        if (ctx.SCRIPTLET() != null)
            scriptletOrSeaWs.setSCRIPTLET(ctx.SCRIPTLET().toString());
        else
            scriptletOrSeaWs.setSEA_WS(ctx.SEA_WS().toString());
        return scriptletOrSeaWs;
    }

    @Override
    public HtmlMisc visitHtmlMisc(HTMLParser.HtmlMiscContext ctx) {
        System.out.println("visitHtmlMisc");
        HtmlMisc htmlMisc = new HtmlMisc();
        htmlMisc.setScopeID(getCurrentScope().getId());
        if (ctx.htmlComment() != null)
            htmlMisc.setHtmlComment(visitHtmlComment(ctx.htmlComment()));
        else
            htmlMisc.setSEA_WS(ctx.SEA_WS().toString());
        return htmlMisc;
    }

    @Override
    public AnotherArrayElement visitAnotherArrayElement(HTMLParser.AnotherArrayElementContext ctx) {
        System.out.println("visitAnotherArrayElement");
        AnotherArrayElement anotherArrayElement = new AnotherArrayElement();
        anotherArrayElement.setScopeID(getCurrentScope().getId());
        anotherArrayElement.setExpression(visitExpression(ctx.expression()));
        anotherArrayElement.setCOMMA(ctx.COMMA().toString());
        return anotherArrayElement;
    }

    @Override
    public Array visitArray(HTMLParser.ArrayContext ctx) {
        System.out.println("visitArray");
        Array array = new Array();
        array.setScopeID(getCurrentScope().getId());
        if (ctx.ARRAYOPEN() != null) {
            array.setARRAYOPEN(ctx.ARRAYOPEN().toString());

            array.setExpression(visitExpression(ctx.expression()));

            for (int i = 0; i < ctx.anotherArrayElement().size(); i++) {
                array.getAnotherArrayElement().add(visitAnotherArrayElement(ctx.anotherArrayElement(i)));
            }
            array.setARRAYCLOSE(ctx.ARRAYCLOSE().toString());
        } else {
            array.setBRACOPEN(ctx.BRACOPEN().toString());
            array.setArray(visitArray(ctx.array()));
            array.setBRACCLOSE(ctx.BRACCLOSE().toString());
        }
        return array;
    }

    @Override
    public Array_element visitArray_element(HTMLParser.Array_elementContext ctx) {
        System.out.println("visitArray_element");
        Array_element array_element = new Array_element();
        array_element.setScopeID(getCurrentScope().getId());
        array_element.setVARNAME(ctx.VARNAME().toString());
        for (int i = 0; i < ctx.arrayElementExp().size(); i++) {
            array_element.getArrayElementExp().add(visitArrayElementExp(ctx.arrayElementExp(i)));
        }
        return array_element;
    }

    @Override
    public ArrayElementExp visitArrayElementExp(HTMLParser.ArrayElementExpContext ctx) {
        System.out.println("visitArrayElementExp");
        ArrayElementExp arrayElementExp = new ArrayElementExp();
        arrayElementExp.setScopeID(getCurrentScope().getId());
        for (int i = 0; i < ctx.function_expr().size(); i++) {
            arrayElementExp.getFunction_expr().add(visitFunction_expr(ctx.function_expr(i)));
        }

        for (int i = 0; i < ctx.arrayIndex().size(); i++) {
            arrayElementExp.getArrayIndex().add(visitArrayIndex(ctx.arrayIndex(i)));
        }
        return arrayElementExp;
    }

    @Override
    public ArrayIndex visitArrayIndex(HTMLParser.ArrayIndexContext ctx) {
        System.out.println("visitArrayIndex");
        ArrayIndex arrayIndex = new ArrayIndex();
        arrayIndex.setScopeID(getCurrentScope().getId());
        arrayIndex.setARRAYOPEN(ctx.ARRAYOPEN().toString());
        if (ctx.NUMBER() != null) {
            arrayIndex.setNUMBER(ctx.NUMBER().toString());
        } else if (ctx.var() != null) {
            arrayIndex.setVar(visitVar(ctx.var()));
        } else {
            arrayIndex.setFunction(visitFunction(ctx.function()));
        }
        arrayIndex.setARRAYCLOSE(ctx.ARRAYCLOSE().toString());
        return arrayIndex;
    }

    @Override
    public AttributeValue visitAttributeValue(HTMLParser.AttributeValueContext ctx) {
        System.out.println("visitAttributeValue");

        AttributeValue attributeValue = new AttributeValue();
        attributeValue.setScopeID(getCurrentScope().getId());
        attributeValue.setATTVALUE_VALUE(ctx.ATTVALUE_VALUE().toString());
        attributeValue.setTAG_EQUALS(ctx.TAG_EQUALS().toString());
        return attributeValue;
    }

    @Override
    public CpAttribute visitCpAttribute(HTMLParser.CpAttributeContext ctx) {
        System.out.println("visitCpAttribute");
        CpAttribute cpAttribute = new CpAttribute();
        cpAttribute.setScopeID(getCurrentScope().getId());
        if (ctx.cp_show() != null)
            cpAttribute.setCp_show(visitCp_show(ctx.cp_show()));
        else if (ctx.cp_for() != null)
            cpAttribute.setCp_for(visitCp_for(ctx.cp_for()));
        else if (ctx.cp_if() != null)
            cpAttribute.setCp_if(visitCp_if(ctx.cp_if()));
        else if (ctx.cp_hide() != null)
            cpAttribute.setCp_hide(visitCp_hide(ctx.cp_hide()));
        else if (ctx.cp_switch() != null)
            cpAttribute.setCp_switch(visitCp_switch(ctx.cp_switch()));
        else if (ctx.cp_switch_case() != null)
            cpAttribute.setCp_switch_case(visitCp_switch_case(ctx.cp_switch_case()));
        else if (ctx.cp_switchDefault() != null)
            cpAttribute.setCp_switchDefault(visitCp_switchDefault(ctx.cp_switchDefault()));
        else
            cpAttribute.setEvent(visitEvent(ctx.event()));
        return cpAttribute;
    }

    @Override
    public ElementAttribute visitElementAttribute(HTMLParser.ElementAttributeContext ctx) {
        System.out.println("visitElementAttribute");
        ElementAttribute elementAttribute = new ElementAttribute();
        elementAttribute.setScopeID(getCurrentScope().getId());
        if (ctx.htmlAttribute() != null)//or is empty
            elementAttribute.setHtmlAttribute(visitHtmlAttribute(ctx.htmlAttribute()));
        else
            elementAttribute.setCpAttribute(visitCpAttribute(ctx.cpAttribute()));
        return elementAttribute;
    }

    @Override
    public Binding visitBinding(HTMLParser.BindingContext ctx) {
        System.out.println("visitBinding");

        Binding binding = new Binding();
        binding.setScopeID(getCurrentScope().getId());
        binding.setBINDING_OPEN(ctx.BINDING_OPEN().toString());
        if (!ctx.binding_exp().isEmpty())
            for (int i = 0; i < ctx.binding_exp().size(); i++)
                binding.getBinding_exp().add(visitBinding_exp(ctx.binding_exp(i)));
        else
            for (int i = 0; i < ctx.expression().size(); i++)
                binding.getExpression().add(visitExpression(ctx.expression(i)));

        binding.setBINDING_CLOSE(ctx.BINDING_CLOSE().toString());


        return binding;
    }

    @Override
    public Binding_exp visitBinding_exp(HTMLParser.Binding_expContext ctx) {
        System.out.println("visitBinding_exp");
        Binding_exp binding_exp = new Binding_exp();
        binding_exp.setScopeID(getCurrentScope().getId());
        if (ctx.expr_Bin() != null)
            binding_exp.setExpr_bin(visitExpr_Bin(ctx.expr_Bin()));
        else
            binding_exp.setExpression(visitExpression(ctx.expression()));
        for (int i = 0; i < ctx.bindingPipe().size(); i++) {
            binding_exp.getBindingPipe().add(visitBindingPipe(ctx.bindingPipe(i)));
        }
        return binding_exp;
    }

    @Override
    public BindingPipe visitBindingPipe(HTMLParser.BindingPipeContext ctx) {
        System.out.println("visitBindingPipe");
        BindingPipe bindingPipe = new BindingPipe();
        bindingPipe.setScopeID(getCurrentScope().getId());
        bindingPipe.setPIPE(ctx.PIPE().toString());
        if (ctx.expr_Bin() != null)
            bindingPipe.setExpr_bin(visitExpr_Bin(ctx.expr_Bin()));
        else
            bindingPipe.setExpression(visitExpression(ctx.expression()));
        return bindingPipe;
    }

    @Override
    public Expr_Bin visitExpr_Bin(HTMLParser.Expr_BinContext ctx) {
        System.out.println("visitExpr_Bin");
        Expr_Bin expr_bin = new Expr_Bin();
        expr_bin.setScopeID(getCurrentScope().getId());
        expr_bin.setVar(visitVar(ctx.var()));
        expr_bin.setCOLUMN_B(ctx.COLUMN_B().toString());
        return expr_bin;
    }

    @Override
    public Event visitEvent(HTMLParser.EventContext ctx) {
        System.out.println("visitEvent");
        Event event = new Event();
        event.setEVENT_NAME(ctx.EVENT_NAME().toString());
        event.setEQUALL(ctx.EQUALL().toString());
        event.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());
        event.setFunction(visitFunction(ctx.function()));
        event.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());
        return event;
    }

    @Override
    public Expression visitExpression(HTMLParser.ExpressionContext ctx) {
        System.out.println("visitExpression");
        Expression expression = new Expression();
        expression.setScopeID(getCurrentScope().getId());
        if (ctx.var() != null)
            expression.setVar(visitVar(ctx.var()));
        else if (ctx.constt() != null)
            expression.setConstt(visitConstt(ctx.constt()));
        else if (ctx.array() != null)
            expression.setArray(visitArray(ctx.array()));
        else if (ctx.logic() != null)
            expression.setLogic(visitLogic(ctx.logic()));
        else if (ctx.one_line_exp() != null)
            expression.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        else if (ctx.objectt() != null)
            expression.setObjectt(visitObjectt(ctx.objectt()));
        else if (ctx.boolen() != null)
            expression.setBoolen(visitBoolen(ctx.boolen()));
        else {
            expression.setOne_line_condition(visitOne_line_condition(ctx.one_line_condition()));
        }

        return expression;
    }

    @Override
    public One_line_exp visitOne_line_exp(HTMLParser.One_line_expContext ctx) {
        System.out.println("visitOne_line_exp");
        One_line_exp one_line_exp = new One_line_exp();
        one_line_exp.setScopeID(getCurrentScope().getId());
        if (ctx.var() != null)
            one_line_exp.setVar(visitVar(ctx.var()));
        else if (ctx.TRUE() != null)
            one_line_exp.setTRUE(ctx.TRUE().toString());
        else if (ctx.FALSE() != null)
            one_line_exp.setFALSE(ctx.FALSE().toString());
        else if (ctx.logic() != null)
            one_line_exp.setLogic(visitLogic(ctx.logic()));
        else if (ctx.function() != null)
            one_line_exp.setFunction(visitFunction(ctx.function()));
        else if (ctx.boolen() != null)
            one_line_exp.setBoolen(visitBoolen(ctx.boolen()));

        one_line_exp.setQMARK(ctx.QMARK().toString());
        one_line_exp.setExpression(visitExpression(ctx.expression(0)));
        one_line_exp.setCOLUMN(ctx.COLUMN().toString());
        one_line_exp.setExpressionSecond(visitExpression(ctx.expression(1)));
        return one_line_exp;
    }

    //Check Expression
    @Override
    public AfterIN visitAfterIN(HTMLParser.AfterINContext ctx) {
        System.out.println("visitAfterIN");
        AfterIN afterIN = new AfterIN();
        afterIN.setScopeID(getCurrentScope().getId());
        if (ctx.var() != null)
            afterIN.setVar(visitVar(ctx.var()));
        else if (ctx.array() != null)
            afterIN.setArray(visitArray(ctx.array()));
        else
            afterIN.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        return afterIN;
    }

    //we have here Error in parser
    @Override
    public AfterSemiColonn visitAfterSemiColonn(HTMLParser.AfterSemiColonnContext ctx) {
        System.out.println("visitAfterSemiColonn");
        AfterSemiColonn afterSemiColonn = new AfterSemiColonn();
        afterSemiColonn.setScopeID(getCurrentScope().getId());
        afterSemiColonn.setSEMICOLONN(ctx.SEMICOLONN().toString());
        if (ctx.var(0) != null)
            afterSemiColonn.setVar(visitVar(ctx.var(0)));
        else if (ctx.one_line_exp(0) != null)
            afterSemiColonn.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp(0)));

        afterSemiColonn.setEQUAL(ctx.EQUAL().toString());

        if (ctx.INDEX() != null)
            afterSemiColonn.setINDEX(ctx.INDEX().toString());
        else if (ctx.var(1) != null)
            afterSemiColonn.setVarSecond(visitVar(ctx.var(1)));
        else if (ctx.NUMBER() != null)
            afterSemiColonn.setNUMBER(ctx.NUMBER().toString());
        else if (ctx.one_line_exp(1) != null)
            afterSemiColonn.setOne_line_expSecond(visitOne_line_exp(ctx.one_line_exp(1)));

        return afterSemiColonn;
    }

    @Override
    public BeforIN visitBeforIN(HTMLParser.BeforINContext ctx) {
        System.out.println("visitBeforIN");

        BeforIN beforIN = new BeforIN();
        beforIN.setScopeID(getCurrentScope().getId());
        if (ctx.var() != null)
            beforIN.setVar(visitVar(ctx.var()));
        else if (ctx.one_line_exp() != null)
            beforIN.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        return beforIN;
    }

    @Override
    public Cp_for visitCp_for(HTMLParser.Cp_forContext ctx) {

        System.out.println("visitCp_for");
        Cp_for cp_for = new Cp_for();
        cp_for.setScopeID(getCurrentScope().getId());
        creatScope();
        cp_for.setCP_FOR(ctx.CP_FOR().toString());
        cp_for.setEQUALL(ctx.EQUALL().toString());
        cp_for.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());

        if (ctx.for_ExprFirst() != null)
            cp_for.setFor_expr(visitFor_ExprFirst(ctx.for_ExprFirst()));
        else if (ctx.for_ExprSecond() != null)
            cp_for.setFor_exprSecond(visitFor_ExprSecond(ctx.for_ExprSecond()));
        else
            cp_for.setExpression(visitExpression(ctx.expression()));
        cp_for.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());

        //removeScope();
        return cp_for;
    }

    @Override
    public for_ExprFirst visitFor_ExprFirst(HTMLParser.For_ExprFirstContext ctx) {
        System.out.println("visitFor_ExprFirst");
        for_ExprFirst for_exprFirst = new for_ExprFirst();
        for_exprFirst.setScopeID(getCurrentScope().getId());
        for_exprFirst.setBeforIN(visitBeforIN(ctx.beforIN()));
        for_exprFirst.setIN(ctx.IN().toString());
        for_exprFirst.setAfterIN(visitAfterIN(ctx.afterIN()));
        if (ctx.afterSemiColonn() != null)
            for_exprFirst.setAfterSemiColonn(visitAfterSemiColonn(ctx.afterSemiColonn()));

        return for_exprFirst;
    }

    @Override
    public for_ExprSecond visitFor_ExprSecond(HTMLParser.For_ExprSecondContext ctx) {
        System.out.println("visitFor_ExprSecond");

        for_ExprSecond for_exprSecond = new for_ExprSecond();
        for_exprSecond.setScopeID(getCurrentScope().getId());
        for_exprSecond.setBeforIN(visitBeforIN(ctx.beforIN(0)));
        for_exprSecond.setCOMMA(ctx.COMMA().toString());
        for_exprSecond.setBeforINSecond(visitBeforIN(ctx.beforIN(1)));
        for_exprSecond.setIN(ctx.IN().toString());
        for_exprSecond.setAfterIN(visitAfterIN(ctx.afterIN()));

        return for_exprSecond;

    }

    @Override
    public AnotherFunctionParameter visitAnotherFunctionParameter(HTMLParser.AnotherFunctionParameterContext ctx) {
        System.out.println("visitAnotherFunctionParameter");

        AnotherFunctionParameter anotherFunctionParameter = new AnotherFunctionParameter();
        anotherFunctionParameter.setScopeID(getCurrentScope().getId());
        anotherFunctionParameter.setCOMMA(ctx.COMMA().toString());
        if (ctx.expression() != null)
            anotherFunctionParameter.setExpression(visitExpression(ctx.expression()));
        else if (ctx.one_line_exp() != null)
            anotherFunctionParameter.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));

        return anotherFunctionParameter;
    }

    @Override
    public Function visitFunction(HTMLParser.FunctionContext ctx) {
        System.out.println("visitFunction");

        Function function = new Function();
        function.setScopeID(getCurrentScope().getId());
        if (ctx.VARNAME() != null)
            function.setVARNAME(ctx.VARNAME().toString());
        else
            function.setArray_element(visitArray_element(ctx.array_element()));

        for (int i = 0; i < ctx.function_expr().size(); i++) {
            function.getFunction_expr().add(visitFunction_expr(ctx.function_expr(i)));
        }


        return function;
    }

    @Override
    public Function_expr visitFunction_expr(HTMLParser.Function_exprContext ctx) {
        System.out.println("visitFunction_expr");

        Function_expr function_expr = new Function_expr();
        function_expr.setScopeID(getCurrentScope().getId());
        function_expr.setBRACOPEN(ctx.BRACOPEN().toString());
        for (int i = 0; i < ctx.functionParameter().size(); i++) {
            function_expr.getFunctionParameter().add(visitFunctionParameter(ctx.functionParameter(i)));
        }
        function_expr.setBRACCLOSE(ctx.BRACCLOSE().toString());

        return function_expr;

    }

    @Override
    public FunctionParameter visitFunctionParameter(HTMLParser.FunctionParameterContext ctx) {
        System.out.println("visitFunctionParameter");

        FunctionParameter functionParameter = new FunctionParameter();
        functionParameter.setScopeID(getCurrentScope().getId());
        if (ctx.expression() != null)
            functionParameter.setExpression(visitExpression(ctx.expression()));
        else
            functionParameter.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));

        for (int i = 0; i < ctx.anotherFunctionParameter().size(); i++) {
            functionParameter.getAnotherFunctionParameter().add(visitAnotherFunctionParameter(ctx.anotherFunctionParameter(i)));
        }
        return functionParameter;
    }

    @Override
    public Cp_hide visitCp_hide(HTMLParser.Cp_hideContext ctx) {
        System.out.println("visitCp_hide");


        Cp_hide cp_hide = new Cp_hide();
        cp_hide.setScopeID(getCurrentScope().getId());
        cp_hide.setCP_HIDE(ctx.CP_HIDE().toString());
        cp_hide.setEQUALL(ctx.EQUALL().toString());
        cp_hide.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());
        cp_hide.setExpression(visitExpression(ctx.expression()));
        cp_hide.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());


        return cp_hide;

    }

    @Override
    public ContainerHtmlContent visitContainerHtmlContent(HTMLParser.ContainerHtmlContentContext ctx) {
        System.out.println("visitContainerHtmlContent");

        ContainerHtmlContent containerHtmlContent = new ContainerHtmlContent();
        containerHtmlContent.setScopeID(getCurrentScope().getId());
        containerHtmlContent.setHtmlContent(visitHtmlContent(ctx.htmlContent()));
        containerHtmlContent.setTAG_OPEN(ctx.TAG_OPEN().toString());
        containerHtmlContent.setTAG_SLASH(ctx.TAG_SLASH().toString());
        containerHtmlContent.setTAG_NAME(ctx.TAG_NAME().toString());
        containerHtmlContent.setTAG_CLOSE(ctx.TAG_CLOSE().toString());

        return containerHtmlContent;
    }

    @Override
    public HtmlAttribute visitHtmlAttribute(HTMLParser.HtmlAttributeContext ctx) {
        System.out.println("visitHtmlAttribute");
//        creatScope();
        HtmlAttribute htmlAttribute = new HtmlAttribute();
        htmlAttribute.setScopeID(getCurrentScope().getId());
        htmlAttribute.setTAG_NAME(ctx.TAG_NAME().toString());
        if (ctx.attributeValue() != null)
            htmlAttribute.setAttributeValue(visitAttributeValue(ctx.attributeValue()));

//        removeScope();
        return htmlAttribute;
    }

    @Override
    public HtmlChardata visitHtmlChardata(HTMLParser.HtmlChardataContext ctx) {
        System.out.println("visitHtmlChardata");

        HtmlChardata htmlChardata = new HtmlChardata();
        htmlChardata.setScopeID(getCurrentScope().getId());
        if (ctx.HTML_TEXT() != null)
            htmlChardata.setHTML_TEXT(ctx.HTML_TEXT().toString());
        else if (ctx.SEA_WS() != null)
            htmlChardata.setSEA_WS(ctx.SEA_WS().toString());

        return htmlChardata;
    }

    @Override
    public HtmlComment visitHtmlComment(HTMLParser.HtmlCommentContext ctx) {
        System.out.println("visitHtmlComment");

        HtmlComment htmlComment = new HtmlComment();
        htmlComment.setScopeID(getCurrentScope().getId());
        if (ctx.HTML_COMMENT() != null)
            htmlComment.setHTML_COMMENT(ctx.HTML_COMMENT().toString());
        else
            htmlComment.setHTML_CONDITIONAL_COMMENT(ctx.HTML_CONDITIONAL_COMMENT().toString());

        return htmlComment;


    }

    @Override
    public HtmlContent visitHtmlContent(HTMLParser.HtmlContentContext ctx) {
        System.out.println("visitHtmlContent");

        HtmlContent htmlContent = new HtmlContent();
        htmlContent.setScopeID(getCurrentScope().getId());
        if (ctx.htmlChardata() != null)
            htmlContent.setHtmlChardata(visitHtmlChardata(ctx.htmlChardata()));

        for (int i = 0; i < ctx.htmlStructure().size(); i++) {
            htmlContent.getHtmlStructure().add(visitHtmlStructure(ctx.htmlStructure(i)));
        }
        return htmlContent;
    }

    @Override
    public HtmlElement visitHtmlElement(HTMLParser.HtmlElementContext ctx) {
        System.out.println("visitHtmlElement");
        HtmlElement htmlElement = new HtmlElement();
        Scope tagScope = getCurrentScope();
        htmlElement.setScopeID(tagScope.getId());
        if (ctx.TAG_OPEN() != null) {
            htmlElement.setTAG_OPEN(ctx.TAG_OPEN().toString());
            htmlElement.setTAG_NAME(ctx.TAG_NAME().toString());
            for (int i = 0; i < ctx.elementAttribute().size(); i++) {
                htmlElement.getElementAttributes().add(visitElementAttribute(ctx.elementAttribute(i)));
            }
            if (ctx.TAG_CLOSE() != null) {
                htmlElement.setTAG_CLOSE(ctx.TAG_CLOSE().toString());
                System.out.println("Test");
                System.out.println(getCurrentScope().getId());
                System.out.println(tagScope.getId());
                System.out.println("test");
                createTag(ctx.TAG_NAME().toString(), tagScope, getCurrentScope().getId() - tagScope.getId(), false);
                if (ctx.containerHtmlContent() != null) {
                    htmlElement.setContainerHtmlContent(visitContainerHtmlContent(ctx.containerHtmlContent()));
                    createTag(htmlElement.getContainerHtmlContent().getTAG_NAME(), getCurrentScope(), 0, true);
                }
            } else {
                htmlElement.setTAG_SLASH_CLOSE(ctx.TAG_SLASH_CLOSE().toString());
            }
        } else if (ctx.SCRIPTLET() != null)
            htmlElement.setSCRIPTLET(ctx.SCRIPTLET().toString());
        else if (ctx.script() != null)
            htmlElement.setScript(visitScript(ctx.script()));
        else
            htmlElement.setStyle(visitStyle(ctx.style()));
        return htmlElement;
    }

    @Override
    public HtmlStructure visitHtmlStructure(HTMLParser.HtmlStructureContext ctx) {
        System.out.println("visitHtmlStructure");

        HtmlStructure htmlStructure = new HtmlStructure();
        htmlStructure.setScopeID(getCurrentScope().getId());
        if (ctx.htmlElement() != null)
            htmlStructure.setHtmlElement(visitHtmlElement(ctx.htmlElement()));
        else if (ctx.CDATA() != null)
            htmlStructure.setCDATA(ctx.CDATA().toString());
        else if (ctx.htmlComment() != null)
            htmlStructure.setHtmlComment(visitHtmlComment(ctx.htmlComment()));
        else if (ctx.binding() != null)
            htmlStructure.setBinding(visitBinding(ctx.binding()));

        if (ctx.htmlChardata() != null)
            htmlStructure.setHtmlChardata(visitHtmlChardata(ctx.htmlChardata()));
        return htmlStructure;

    }

    @Override
    public Script visitScript(HTMLParser.ScriptContext ctx) {
        System.out.println("visitScript");

        Script script = new Script();
        script.setScopeID(getCurrentScope().getId());
        script.setSCRIPT_OPEN(ctx.SCRIPT_OPEN().toString());
        if (ctx.SCRIPT_BODY() != null)
            script.setSCRIPT_BODY(ctx.SCRIPT_BODY().toString());
        else if (ctx.SCRIPT_SHORT_BODY() != null)
            script.setSCRIPT_SHORT_BODY(ctx.SCRIPT_SHORT_BODY().toString());

        return script;
    }

    @Override
    public Style visitStyle(HTMLParser.StyleContext ctx) {
        System.out.println("visitStyle");

        Style style = new Style();
        style.setScopeID(getCurrentScope().getId());
        style.setSTYLE_OPEN(ctx.STYLE_OPEN().toString());
        if (ctx.STYLE_BODY() != null)
            style.setSTYLE_BODY(ctx.STYLE_BODY().toString());
        else if (ctx.STYLE_SHORT_BODY() != null)
            style.setSTYLE_SHORT_BODY(ctx.STYLE_SHORT_BODY().toString());

        return style;

    }

    @Override
    public Cp_if visitCp_if(HTMLParser.Cp_ifContext ctx) {
        System.out.println("visitCp_if");
        Cp_if cp_if = new Cp_if();
        cp_if.setScopeID(getCurrentScope().getId());
        creatScope();

        cp_if.setCP_IF(ctx.CP_IF().toString());
        cp_if.setEQUALL(ctx.EQUALL().toString());
        cp_if.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());
        cp_if.setExpression(visitExpression(ctx.expression()));
        cp_if.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());

//        removeScope();
        return cp_if;
    }

    @Override
    public One_line_condition visitOne_line_condition(HTMLParser.One_line_conditionContext ctx) {
        System.out.println("visitOne_line_condition");

        One_line_condition one_line_condition = new One_line_condition();
        one_line_condition.setScopeID(getCurrentScope().getId());
        one_line_condition.setBRACOPEN(ctx.BRACOPEN().toString());
        one_line_condition.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        one_line_condition.setBRACCLOSE(ctx.BRACCLOSE().toString());

        return one_line_condition;

    }

    @Override
    public AnotherObj visitAnotherObj(HTMLParser.AnotherObjContext ctx) {
        System.out.println("visitOne_line_condition");

        AnotherObj anotherObj = new AnotherObj();
        anotherObj.setScopeID(getCurrentScope().getId());
        anotherObj.setCOMMA(ctx.COMMA().toString());
        anotherObj.setExpr_object(visitExpr_object(ctx.expr_object()));
        return anotherObj;
    }

    @Override
    public Expr_object visitExpr_object(HTMLParser.Expr_objectContext ctx) {
        System.out.println("visitExpr_object");

        Expr_object expr_object = new Expr_object();
        expr_object.setScopeID(getCurrentScope().getId());
        expr_object.setVARNAME(ctx.VARNAME().toString());
        expr_object.setCOLUMN(ctx.COLUMN().toString());
        expr_object.setExpression(visitExpression(ctx.expression()));

        return expr_object;

    }

    @Override
    public Objectt visitObjectt(HTMLParser.ObjecttContext ctx) {
        System.out.println("visitObjectt");

        Objectt objectt = new Objectt();
        objectt.setScopeID(getCurrentScope().getId());
        objectt.setCURLYOPEN(ctx.CURLYOPEN().toString());
        objectt.setExpr_object(visitExpr_object(ctx.expr_object()));
        for (int i = 0; i < ctx.anotherObj().size(); i++) {
            objectt.getAnotherObj().add(visitAnotherObj(ctx.anotherObj(i)));
        }
        objectt.setCURLYCLOSE(ctx.CURLYCLOSE().toString());

        return objectt;
    }

    @Override
    public Cp_show visitCp_show(HTMLParser.Cp_showContext ctx) {
        System.out.println("visitCp_show");


        Cp_show cp_show = new Cp_show();
        cp_show.setScopeID(getCurrentScope().getId());
        cp_show.setCP_SHOW(ctx.CP_SHOW().toString());
        cp_show.setEQUALL(ctx.EQUALL().toString());
        cp_show.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());
        cp_show.setExpression(visitExpression(ctx.expression()));
        cp_show.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());


        return cp_show;


    }

    @Override
    public Cp_switch_case visitCp_switch_case(HTMLParser.Cp_switch_caseContext ctx) {
        System.out.println("visitCp_switch_case");

        creatScope();
        Cp_switch_case cp_switch_case = new Cp_switch_case();
        cp_switch_case.setScopeID(getCurrentScope().getId());
        cp_switch_case.setCP_SWITCH_CASE(ctx.CP_SWITCH_CASE().toString());
        cp_switch_case.setEQUALL(ctx.EQUALL().toString());
        cp_switch_case.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());

        if (ctx.constt() != null)
            cp_switch_case.setConstt(visitConstt(ctx.constt()));
        else if (ctx.var() != null)
            cp_switch_case.setVar(visitVar(ctx.var()));
        else if (ctx.one_line_exp() != null)
            cp_switch_case.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        else if (ctx.logic() != null)
            cp_switch_case.setLogic(visitLogic(ctx.logic()));

        cp_switch_case.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());

//        removeScope();
        return cp_switch_case;

    }

    @Override
    public Cp_switch visitCp_switch(HTMLParser.Cp_switchContext ctx) {
        System.out.println("visitCp_switch");


        Cp_switch cp_switch = new Cp_switch();
        cp_switch.setScopeID(getCurrentScope().getId());
        cp_switch.setCP_SWITCH(ctx.CP_SWITCH().toString());
        cp_switch.setEQUALL(ctx.EQUALL().toString());
        cp_switch.setQUOTETION_OPEN(ctx.QUOTETION_OPEN().toString());
        if (ctx.var() != null)
            cp_switch.setVar(visitVar(ctx.var()));
        else
            cp_switch.setOne_line_exp(visitOne_line_exp(ctx.one_line_exp()));
        cp_switch.setQUOTETION_CLOSE(ctx.QUOTETION_CLOSE().toString());


        return cp_switch;
    }

    @Override
    public Cp_switchDefault visitCp_switchDefault(HTMLParser.Cp_switchDefaultContext ctx) {
        System.out.println("visitCp_switchDefault");

        Cp_switchDefault cp_switchDefault = new Cp_switchDefault();
        cp_switchDefault.setScopeID(getCurrentScope().getId());
        creatScope();
        cp_switchDefault.setCP_SWITCHDEFAULT(ctx.CP_SWITCHDEFAULT().toString());
//        removeScope();
        return cp_switchDefault;
    }

    @Override
    public Constt visitConstt(HTMLParser.ConsttContext ctx) {
        System.out.println("visitConstt");

        Constt constt = new Constt();
        constt.setScopeID(getCurrentScope().getId());


        if (ctx.CHAR() != null)
            constt.setCHAR(ctx.CHAR().toString());
        else if (ctx.NUMBER() != null)
            constt.setNUMBER(ctx.NUMBER().toString());
        else if (ctx.mathOperation() != null)
            constt.setMathOperation(visitMathOperation(ctx.mathOperation()));
        else if (ctx.function() != null)
            constt.setFunction(visitFunction(ctx.function()));
        else if (ctx.constOperation() != null)
            constt.setConstOperation(visitConstOperation(ctx.constOperation()));
        else {
            constt.setBRACOPEN(ctx.BRACOPEN().toString());
            constt.setConstt(visitConstt(ctx.constt()));
            constt.setBRACCLOSE(ctx.BRACCLOSE().toString());
        }

        return constt;
    }

    @Override
    public ConstOperation visitConstOperation(HTMLParser.ConstOperationContext ctx) {
        System.out.println("visitConstOperation");
        ConstOperation constOperation = new ConstOperation();
        constOperation.setScopeID(getCurrentScope().getId());
        if (ctx.CHAR() != null) {
            constOperation.setCHAR(ctx.CHAR().toString());
        } else if (ctx.var() != null) {
            constOperation.setVar(visitVar(ctx.var()));
        } else {
            constOperation.setFunction(visitFunction(ctx.function()));
        }
        for (int i = 0; i < ctx.constSide().size(); i++)
            constOperation.getConstSide().add(visitConstSide(ctx.constSide(i)));
        return constOperation;
    }

    @Override
    public ConstSide visitConstSide(HTMLParser.ConstSideContext ctx) {
        System.out.println("visitConstSide");
        ConstSide constSide = new ConstSide();
        constSide.setScopeID(getCurrentScope().getId());
        if (ctx.PLUS() != null)
            constSide.setPLUS(ctx.PLUS().toString());
        else
            constSide.setMINUS(ctx.MINUS().toString());
        constSide.setConstOperation(visitConstOperation(ctx.constOperation()));
        return constSide;
    }

    @Override
    public logic visitLogic(HTMLParser.LogicContext ctx) {
        System.out.println("visit Logic");
        logic logic = new logic();
        logic.setScopeID(getCurrentScope().getId());
        if (ctx.boolen() != null) {
            logic.setBoolen(visitBoolen(ctx.boolen()));
            for (int i = 0; i < ctx.logicSide().size(); i++) {
                logic.getLogicSides().add(visitLogicSide(ctx.logicSide(i)));
            }
        } else if (ctx.var() != null) {
            logic.setVar(visitVar(ctx.var()));
            for (int i = 0; i < ctx.logicSide().size(); i++) {
                logic.getLogicSides().add(visitLogicSide(ctx.logicSide(i)));
            }
        } else {
            if (ctx.NOT() != null) {
                logic.setNOT(ctx.NOT().toString());
            }
            logic.setBRACOPEN(ctx.BRACOPEN().toString());
            logic.setLogic(visitLogic(ctx.logic()));
            logic.setBRACCLOSE(ctx.BRACCLOSE().toString());
        }
        return logic;
    }

    @Override
    public logicSide visitLogicSide(HTMLParser.LogicSideContext ctx) {
        System.out.println("visit LogicSide");
        logicSide logicSide = new logicSide();
        logicSide.setScopeID(getCurrentScope().getId());
        logicSide.setLOGIC(ctx.LOGIC().toString());
        if (ctx.boolen() != null)
            logicSide.setBoolen(visitBoolen(ctx.boolen()));
        else
            logicSide.setVar(visitVar(ctx.var()));
        return logicSide;

    }

    @Override
    public MathOperation visitMathOperation(HTMLParser.MathOperationContext ctx) {
        System.out.println("visit Math Operation");
        MathOperation mathOperation = new MathOperation();
        mathOperation.setScopeID(getCurrentScope().getId());
        if (ctx.operationSide() != null) {
            mathOperation.setOperationSide(visitOperationSide(ctx.operationSide()));
            for (int i = 0; i < ctx.operationSideWithPlusMinus().size(); i++) {
                mathOperation.getOperationSideWithPlusMinuses().add(visitOperationSideWithPlusMinus(ctx.operationSideWithPlusMinus(i)));
            }
        } else if (ctx.operationElement() != null) {
            mathOperation.setOperationElement(visitOperationElement(ctx.operationElement()));

            if (ctx.PLUSPLUS() != null)
                mathOperation.setPLUSPLUS(ctx.PLUSPLUS().toString());
            else
                mathOperation.setMINUSMINUS(ctx.MINUSMINUS().toString());
        }

        return mathOperation;

    }

    @Override
    public OperationSideWithPlusMinus visitOperationSideWithPlusMinus(HTMLParser.OperationSideWithPlusMinusContext ctx) {
        System.out.println("visit OperationSideWithPlusMinus");
        OperationSideWithPlusMinus operationSideWithPlusMinus = new OperationSideWithPlusMinus();
        operationSideWithPlusMinus.setScopeID(getCurrentScope().getId());
        if (ctx.PLUS() != null)
            operationSideWithPlusMinus.setPLUS(ctx.PLUS().toString());
        else
            operationSideWithPlusMinus.setMINUS(ctx.MINUS().toString());

        operationSideWithPlusMinus.setOperationSide(visitOperationSide(ctx.operationSide()));

        return operationSideWithPlusMinus;
    }

    @Override
    public OperationSide visitOperationSide(HTMLParser.OperationSideContext ctx) {
        System.out.println("visit OperationSide");
        OperationSide operationSide = new OperationSide();
        operationSide.setScopeID(getCurrentScope().getId());
        if (ctx.operationElement() != null) {
            operationSide.setOperationElement(visitOperationElement(ctx.operationElement()));
            for (int i = 0; i < operationSide.getOperationSideWithDivMultRemain().size(); i++) {
                operationSide.getOperationSideWithDivMultRemain().add(visitOperationSideWithDivMultRemain(ctx.operationSideWithDivMultRemain(i)));
            }
        } else {
            operationSide.setBRACOPEN(ctx.BRACOPEN().toString());
            if (ctx.mathOperation() != null) {
                operationSide.setMathOperation(visitMathOperation(ctx.mathOperation()));
                operationSide.setBRACCLOSE(ctx.BRACCLOSE().toString());
                for (int i = 0; i < operationSide.getOperationSideWithDivMultRemain().size(); i++) {
                    operationSide.getOperationSideWithDivMultRemain().add(visitOperationSideWithDivMultRemain(ctx.operationSideWithDivMultRemain(i)));
                }
            } else {
                operationSide.setOperationSide(visitOperationSide(ctx.operationSide()));
                operationSide.setBRACCLOSE(ctx.BRACCLOSE().toString());
            }
        }
        return operationSide;
    }

    @Override
    public operationSideWithDivMultRemain visitOperationSideWithDivMultRemain(HTMLParser.OperationSideWithDivMultRemainContext ctx) {
        System.out.println("visit OperationSideWithDivMultRemain");
        operationSideWithDivMultRemain operationSideWithDivMultRemain = new operationSideWithDivMultRemain();
        operationSideWithDivMultRemain.setScopeID(getCurrentScope().getId());
        if (ctx.DIV() != null)
            operationSideWithDivMultRemain.setDIV(ctx.DIV().toString());
        else if (ctx.MULT() != null)
            operationSideWithDivMultRemain.setMULT(ctx.MULT().toString());
        else
            operationSideWithDivMultRemain.setREMAIN(ctx.REMAIN().toString());

        operationSideWithDivMultRemain.setOperationSide(visitOperationSide(ctx.operationSide()));

        return operationSideWithDivMultRemain;
    }

    @Override
    public OperationElement visitOperationElement(HTMLParser.OperationElementContext ctx) {
        System.out.println("visit OperationElement");

        OperationElement operationElement = new OperationElement();
        operationElement.setScopeID(getCurrentScope().getId());
        if (ctx.NUMBER() != null)
            operationElement.setNUMBER(ctx.NUMBER().toString());
        else if (ctx.var() != null)
            operationElement.setVar(visitVar(ctx.var()));
        else
            operationElement.setFunction(visitFunction(ctx.function()));

        return operationElement;
    }

    @Override
    public DotVar visitDotVar(HTMLParser.DotVarContext ctx) {
        System.out.println("visitDotVar");
        DotVar dotVar = new DotVar();
        dotVar.setDOT(ctx.DOT().toString());
        dotVar.setVar(visitVar(ctx.var()));
        dotVar.setScopeID(dotVar.getVar().getScopeID());
        return dotVar;
    }

    @Override
    public Simple_var visitSimple_var(HTMLParser.Simple_varContext ctx) {
        System.out.println("visitSimple_var");

        Simple_var simple_var = new Simple_var();
        if (ctx.VARNAME() != null) {
            simple_var.setVARNAME(ctx.VARNAME().toString());
            try {
                System.out.println(simple_var.getVARNAME() + "__________________________________");
                int scopeTemp = Main.symbolTable.getSymbolScope(simple_var.getVARNAME()).getId();
                simple_var.setScopeID(scopeTemp);
                System.out.println("Testa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" + scopeTemp);
            } catch (NullPointerException e) {
                simple_var.setScopeID(getCurrentScope().getId());
                System.out.println("Testa aaaaaaaaaaaa");
            }
        } else if (ctx.array_element() != null) {
            simple_var.setArray_element(visitArray_element(ctx.array_element()));
            try {
                int scopeTemp = Main.symbolTable.getSymbolScope(simple_var.getArray_element().getVARNAME()).getId();
                simple_var.setScopeID(scopeTemp);
            } catch (NullPointerException e) {
                simple_var.setScopeID(getCurrentScope().getId());
            }
        } else if (ctx.one_line_condition() != null) {
            simple_var.setOne_line_condition(visitOne_line_condition(ctx.one_line_condition()));
            simple_var.setScopeID(simple_var.getOne_line_condition().getScopeID());
        } else {
            simple_var.setBRACOPEN(ctx.BRACOPEN().toString());
            simple_var.setSimple_var(visitSimple_var(ctx.simple_var()));
            simple_var.setBRACCLOSE(ctx.BRACCLOSE().toString());
            simple_var.setScopeID(simple_var.getSimple_var().getScopeID());
        }

        return simple_var;

    }

    @Override
    public Var visitVar(HTMLParser.VarContext ctx) {
        System.out.println("visitVar");
        Var var = new Var();
        if (ctx.simple_var() != null) {
            var.setSimple_var(visitSimple_var(ctx.simple_var()));
            for (int i = 0; i < ctx.dotVar().size(); i++) {
                var.getDotVars().add(visitDotVar(ctx.dotVar(i)));
            }
            var.setScopeID(var.getSimple_var().getScopeID());
        } else if (ctx.function() != null) {
            var.setFunction(visitFunction(ctx.function()));
            for (int i = 0; i < ctx.dotVar().size(); i++) {
                var.getDotVars().add(visitDotVar(ctx.dotVar(i)));
                var.setScopeID(var.getFunction().getScopeID());
            }
        } else if (ctx.BRACOPEN() != null) {
            var.setBRACOPEN(ctx.BRACOPEN().toString());
            var.setVar(visitVar(ctx.var()));
            var.setBRACCLOSE(ctx.BRACCLOSE().toString());
            var.setScopeID(var.getVar().getScopeID());
        } else {
            var.setAPOSTROPHE1(ctx.APOSTROPHE(0).toString());
            var.setVar(visitVar(ctx.var()));
            var.setScopeID(var.getVar().getScopeID());
            for (int i = 0; i < ctx.COLUMN().size(); i++) {
                var.getCOLUMN().add(ctx.COLUMN(i).toString());
            }
            var.setAPOSTROPHE2(ctx.APOSTROPHE(1).toString());
        }
        return var;

    }

    @Override
    public booleanSide visitBooleanSide(HTMLParser.BooleanSideContext ctx) {
        System.out.println("visitBooleanSide");
        booleanSide booleanSide = new booleanSide();
        booleanSide.setScopeID(getCurrentScope().getId());
        if (ctx.var() != null)
            booleanSide.setVar(visitVar(ctx.var()));
        else if (ctx.NUMBER() != null)
            booleanSide.setNUMBER(ctx.NUMBER().toString());
        else if (ctx.function() != null)
            booleanSide.setFunction(visitFunction(ctx.function()));
        else
            booleanSide.setMathOperation(visitMathOperation(ctx.mathOperation()));
        return booleanSide;
    }

    @Override
    public boolen visitBoolen(HTMLParser.BoolenContext ctx) {
        System.out.println("visitBoolen");
        boolen boolen = new boolen();
        boolen.setScopeID(getCurrentScope().getId());
        if (!ctx.booleanSide().isEmpty()) {
            boolen.setBooleanSide1(visitBooleanSide(ctx.booleanSide().get(0)));
            boolen.setBOOLEAN(ctx.BOOLEAN().toString());
            boolen.setBooleanSide2(visitBooleanSide(ctx.booleanSide().get(1)));
        } else if (ctx.TRUE() != null)
            boolen.setTRUE(ctx.TRUE().toString());
        else if (ctx.FALSE() != null)
            boolen.setFALSE(ctx.FALSE().toString());
        else if (ctx.function() != null)
            boolen.setFunction(visitFunction(ctx.function()));
        else if (ctx.BRACOPEN() != null) {
            boolen.setBRACOPEN(ctx.BRACOPEN().toString());
            boolen.setBoolen(visitBoolen(ctx.boolen()));
            boolen.setBRACCLOSE(ctx.BRACCLOSE().toString());
        } else {
            boolen.setNOT(ctx.NOT().toString());
            if (ctx.boolen() != null)
                boolen.setBoolen(visitBoolen(ctx.boolen()));
            else if (ctx.var() != null)
                boolen.setVar(visitVar(ctx.var()));
            else
                boolen.setFunction(visitFunction(ctx.function()));
        }
        return boolen;
    }

    private void creatScope() {

        System.out.println("Creat New Scope");
        Scope scope;
        System.out.println("ssssss");
        if (Main.symbolTable.getScopeStack().isEmpty()) {
            scope = new Scope(null);
        } else {
            Scope parntScope1 = Main.symbolTable.getScopeStack().peek();
            scope = new Scope(parntScope1);
        }

        Main.symbolTable.getScopeStack().push(scope);
        Main.symbolTable.getScopes().add(scope);
    }

    private void removeScope() {
        Main.symbolTable.getScopeStack().pop();
        System.out.println("remove last Scope from scope stake");
    }

    private Scope getCurrentScope() {
        if (Main.symbolTable.getScopeStack().isEmpty())
            return null;

        return Main.symbolTable.getScopeStack().peek();
    }

    private void createTag(String tagName, Scope tagScope, int scopeCreated, boolean isClose) {
        Main.tagTable.getTagTable().add(new TagInfo(tagName, tagScope, scopeCreated, isClose));
        int sll;
        if (isClose == true) {
            for (int i = Main.tagTable.getTagTable().size() - 1; i >= 0; i--) {
                if (Main.tagTable.getTagTable().get(i).isClose() == false) {
                    Main.tagTable.getTagTable().get(i).setClose(true);
                    int scopesToRemove = Main.tagTable.getTagTable().get(i).getScopeCreated();//semantic check
                    for (int j = 0; j < scopesToRemove; j++) {
                        removeScope();
                    }
                    break;
                }
            }
        }
    }
}








