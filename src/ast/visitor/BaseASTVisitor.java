package ast.visitor;

import SymbolTable.Scope;
import SymbolTable.Symbol;
import ast.model.Array.*;
import ast.model.Attribue.AttributeValue;
import ast.model.Attribue.CpAttribute;
import ast.model.Attribue.ElementAttribute;
import ast.model.Binding.Binding;
import ast.model.Binding.BindingPipe;
import ast.model.Binding.Binding_exp;
import ast.model.Binding.Expr_Bin;
import ast.model.Event.Event;
import ast.model.Expression.Expression;
import ast.model.Expression.One_line_exp;
import ast.model.For.*;
import ast.model.Function.AnotherFunctionParameter;
import ast.model.Function.Function;
import ast.model.Function.FunctionParameter;
import ast.model.Function.Function_expr;
import ast.model.Hide.Cp_hide;
import ast.model.HtmlS.*;
import ast.model.If.Cp_if;
import ast.model.If.One_line_condition;
import ast.model.Object.AnotherObj;
import ast.model.Object.Expr_object;
import ast.model.Object.Objectt;
import ast.model.Operation.*;
import ast.model.Show.Cp_show;
import ast.model.Switch.Cp_switch;
import ast.model.Switch.Cp_switchDefault;
import ast.model.Switch.Cp_switch_case;
import ast.model.Var.*;
import ast.model.booleanSide;
import ast.model.boolen;
import ast.nodes.HTMLElements;
import ast.nodes.HtmlDocument;
import tareq.Main;

public class BaseASTVisitor implements ASTVisitor{

    @Override
    public void visit(HTMLElements htmlElements) {
        System.out.println("HTMLElements");
        System.out.println();
    }

    @Override
    public void visit(Array array) {
        System.out.println("Array");/*
        System.out.println(array.getARRAYOPEN());
        System.out.println(array.getARRAYCLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(AnotherArrayElement anotherArrayElement) {
        System.out.println("AnotherArrayElement");/*
        System.out.println(anotherArrayElement.getCOMMA());*/
        System.out.println();
    }

    @Override
    public void visit(ArrayIndex arrayIndex) {
        System.out.println("ArrayIndex");/*
        System.out.println(arrayIndex.getARRAYOPEN());
        System.out.println(arrayIndex.getNUMBER());
        System.out.println(arrayIndex.getARRAYCLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(ArrayElementExp arrayElementExp) {
        System.out.println("ArrayElementExp");
        System.out.println();
    }

    @Override
    public void visit(Array_element array_element) {
        System.out.println("Array_element");/*
        System.out.println(array_element.getVARNAME());*/
        System.out.println();
    }

    @Override
    public void visit(ElementAttribute elementAttribute) {
        System.out.println("ElementAttribute");
        System.out.println();
    }

    @Override
    public void visit(CpAttribute cpAttribute) {
        System.out.println("CpAttribute");
        System.out.println();
    }

    @Override
    public void visit(AttributeValue attributeValue) {
        System.out.println("AttributeValue");/*
        System.out.println(attributeValue.getTAG_EQUALS());
        System.out.println(attributeValue.getATTVALUE_VALUE());*/
        System.out.println();
    }

    @Override
    public void visit(Expr_Bin expr_bin) {
        System.out.println("Expr_Bin");/*
        System.out.println(expr_bin.getCOLUMN());
        System.out.println(expr_bin.getSTRING());*/
        System.out.println();
    }

    @Override
    public void visit(BindingPipe bindingPipe) {
        System.out.println("BindingPipe");/*
        System.out.println(bindingPipe.getPIPE());*/
        System.out.println();
    }

    @Override
    public void visit(Binding_exp binding_exp) {
        System.out.println("Binding_exp");
        System.out.println();
    }

    @Override
    public void visit(Binding binding) {
        System.out.println("Binding");/*
        System.out.println(binding.getBINDING_OPEN());
        System.out.println(binding.getBINDING_CLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(Event event) {
        System.out.println("Event");/*
        System.out.println(event.getEVENT_NAME());
        System.out.println(event.getEQUALL());
        System.out.println(event.getQUOTETION_OPEN());
        System.out.println(event.getQUOTETION_CLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(Expression expression) {
        System.out.println("Expression");
        System.out.println();
    }

    @Override
    public void visit(One_line_exp one_line_exp) {
        System.out.println("One_line_exp");/*
        System.out.println(one_line_exp.getTRUE());
        System.out.println(one_line_exp.getFALSE());
        System.out.println(one_line_exp.getQMARK());
        System.out.println(one_line_exp.getCOLUMN());*/
        System.out.println();
    }

    @Override
    public void visit(AfterIN afterIN) {
        System.out.println("AfterIN");
        System.out.println();
    }

    @Override
    public void visit(AfterSemiColonn afterSemiColonn) {
        System.out.println("AfterSemiColonn");/*
        System.out.println(afterSemiColonn.getSEMICOLONN());
        System.out.println(afterSemiColonn.getINDEX());
        System.out.println(afterSemiColonn.getNUMBER());*/
        System.out.println();
    }

    @Override
    public void visit(BeforIN beforIN) {
        System.out.println("BeforIN");
        System.out.println();
    }

    @Override
    public void visit(Cp_for cp_for) {
        System.out.println("CP_FOR");/*
        System.out.println(cp_for.getCP_FOR());
        System.out.println(cp_for.getEQUALL());
        System.out.println(cp_for.getQUOTETION_OPEN());
        System.out.println(cp_for.getQUOTETION_CLOSE());*/
        System.out.println();

    }

    @Override
    public void visit(for_ExprFirst for_exprFirst) {
        System.out.println("for_exprFirst");/*
        System.out.println(for_exprFirst.getIN());*/
        System.out.println();
    }

    @Override
    public void visit(for_ExprSecond for_exprSecond) {
        System.out.println("for_exprSecond");/*
        System.out.println(for_exprSecond.getCOMMA());
        System.out.println(for_exprSecond.getIN());*/
        System.out.println();
    }

    @Override
    public void visit(AnotherFunctionParameter anotherFunctionParameter) {
        System.out.println("anotherFunctionParameter");/*
        System.out.println(anotherFunctionParameter.getCOMMA());*/
        System.out.println();
    }

    @Override
    public void visit(Function function) {
        System.out.println("function");/*
        System.out.println(function.getVARNAME());*/
        System.out.println();
        if(function.getVARNAME()!=null)
            createSymbol(function.getScopeID(),function.getVARNAME(),"call function");
        if(function.getArray_element()!=null)
            createSymbol(function.getScopeID(),function.getArray_element().getVARNAME(),"call arrayelment function");
    }

    @Override
    public void visit(Function_expr function_expr) {
        System.out.println("function_expr");/*
        System.out.println(function_expr.getBRACOPEN());
        System.out.println(function_expr.getBRACCLOSE());*/
    }

    @Override
    public void visit(FunctionParameter functionParameter) {
        System.out.println("functionParameter");
        System.out.println();
    }

    @Override
    public void visit(Cp_hide cp_hide) {
        System.out.println("cp_hide");/*
        System.out.println(cp_hide.getCP_HIDE());
        System.out.println(cp_hide.getEQUALL());
        System.out.println(cp_hide.getQUOTETION_OPEN());
        System.out.println(cp_hide.getQUOTETION_CLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(ContainerHtmlContent containerHtmlContent) {
        System.out.println("containerHtmlContent");
        System.out.println();
    }

    @Override
    public void visit(HtmlAttribute htmlAttribute) {
        System.out.println("htmlAttribute");
        System.out.println();
    }

    @Override
    public void visit(HtmlChardata htmlChardata) {
        System.out.println("htmlChardata");
        System.out.println();

    }

    @Override
    public void visit(HtmlComment htmlComment) {
        System.out.println("htmlComment");
        System.out.println();
    }

    @Override
    public void visit(HtmlContent htmlContent) {
        System.out.println("htmlContent");
        System.out.println();
    }

    @Override
    public void visit(HtmlElement htmlElement) {
        System.out.println("HTML Element");/*
        System.out.println(htmlElement.getTAG_OPEN());
        System.out.println(htmlElement.getTAG_NAME());
        System.out.println(htmlElement.getTAG_CLOSE());*/
        System.out.println();
    }

    @Override
    public void visit(HtmlMisc htmlMisc) {
        System.out.println("htmlMisc");
        System.out.println();
    }

    @Override
    public void visit(HtmlStructure htmlStructure) {
        System.out.println("htmlStructure");
        System.out.println();
    }

    @Override
    public void visit(Script script) {
        System.out.println("script");
        System.out.println();
    }

    @Override
    public void visit(ScriptletOrSeaWs scriptletOrSeaWs) {
        System.out.println("scriptletOrSeaWs");
        System.out.println();
    }

    @Override
    public void visit(Style style) {
        System.out.println("style");
        System.out.println();
    }

    @Override
    public void visit(Cp_if cp_if) {
        System.out.println("cp_if");
        System.out.println();
    }

    @Override
    public void visit(One_line_condition one_line_condition) {
        System.out.println("one_line_condition");
        System.out.println();
    }

    @Override
    public void visit(AnotherObj anotherObj) {
        System.out.println("anotherObj");
        System.out.println();
    }

    @Override
    public void visit(Expr_object expr_object) {
        System.out.println("expr_object");
        System.out.println();

        if(expr_object.getVARNAME()!=null)
            createSymbol(expr_object.getScopeID(),expr_object.getVARNAME(),"var");
    }

    @Override
    public void visit(Objectt objectt) {
        System.out.println("object");
        System.out.println();
    }
/*
    @Override
    public void visit(OperationDELETED operation) {
        System.out.println("operation");
        System.out.println();
    }

    @Override
    public void visit(OperationLeftSideDELETED operationLeftSide) {
        System.out.println("operationLeftSide");
        System.out.println();
    }

    @Override
    public void visit(OperationRightSideDELETED operationRightSide) {
        System.out.println("operationRightSide");
        System.out.println();
    }
*/
    @Override
    public void visit(Cp_show cp_show) {
        System.out.println("CP-SHOW");
        System.out.println();
    }

    @Override
    public void visit(Cp_switch cp_switch) {
        System.out.println("cp_switch");
        System.out.println();
    }

    @Override
    public void visit(Cp_switch_case cp_switch_case) {
        System.out.println("cp_switch_case");
        System.out.println();
    }

    @Override
    public void visit(Cp_switchDefault cp_switchDefault) {
        System.out.println("cp_switchDefault");
        System.out.println();
    }

    @Override
    public void visit(Constt constt) {
        System.out.println("const");
        System.out.println();
        if(constt.getCHAR()!=null)
            createSymbol(constt.getScopeID(),constt.getCHAR(),"Char");

    }

    @Override
    public void visit(DotVar dotVar) {
        System.out.println("dotVar");
        System.out.println();
    }

    @Override
    public void visit(Simple_var simple_var) {
        System.out.println("simple_var");
        System.out.println();
        if(simple_var.getVARNAME()!=null) {
            createSymbol(simple_var.getScopeID(), simple_var.getVARNAME(), "var");
            System.out.println( simple_var.getVARNAME() + "====================================");
        }
        if(simple_var.getArray_element()!=null)
            createSymbol(simple_var.getArray_element().getScopeID(),simple_var.getArray_element().getVARNAME(),"array element");
        System.out.println(simple_var.getScopeID());

    }

    @Override
    public void visit(Var var) {
        System.out.println("var");
        System.out.println();
    }

    @Override
    public void visit(HtmlDocument htmlDocument) {
        System.out.println("HTML Document");
        System.out.println();
    }

    @Override
    public void visit (OperationElement OperationElement){
        System.out.println("Operation Element");
        System.out.println();
    }

    @Override
    public void visit (operationSideWithDivMultRemain operationSideWithDivMultRemain){
        System.out.println("operationSide With Div Mult Remain");
        System.out.println();
    }

    @Override
    public void visit (OperationSide operationSide){
        System.out.println("OperationSide");
        System.out.println();
    }

    @Override
    public void visit (OperationSideWithPlusMinus operationSideWithPlusMinus){
        System.out.println("OperationSide With Plus Minus");
        System.out.println();
    }

    @Override
    public void visit(MathOperation mathOperation){
        System.out.println("Math Operation");
        System.out.println();
    }

    @Override
    public void visit (booleanSide booleanSide){
        System.out.println("boolean Side");
        System.out.println();


    }

    @Override
    public void visit (boolen boolen){
        System.out.println("boolen");
        System.out.println();
    }

    @Override
    public void visit (logicSide logicSide){
        System.out.println("logicSide");
        System.out.println();
    }

    @Override
    public void visit (logic logic){
        System.out.println("logic");
        System.out.println();
    }

    @Override
    public void visit(ConstOperation constOperation) {
        System.out.println("ConstOperation");
        System.out.println();
    }

    @Override
    public void visit(ConstSide constSide) {
        System.out.println("constSide");
        System.out.println();
    }

    private void createSymbol(int ScopeId , String name, String type){
        System.out.println("create new Symbol ...");
        Symbol symbol = new Symbol(name);
        symbol.setType(type);
        Scope scope = Main.symbolTable.getScopeByID(ScopeId);
        symbol.setScope(scope);
        scope.addSymbol(name,symbol);
    }
}
