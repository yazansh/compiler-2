package ast.model;

import ast.model.Function.Function;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class boolen extends Node {

    booleanSide booleanSide1;
    String BOOLEAN;
    booleanSide booleanSide2;
    //or
    String TRUE;
    //or
    String FALSE;
    //or
    Function function;
    //or
    String BRACOPEN;
    boolen boolen;
    String BRACCLOSE;
    //or
    String NOT;
    Var var;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Boolean:");
        printSpaces(spaceCount + 2);
        if (booleanSide1 != null) {
            booleanSide1.accept(astVisitor, spaceCount + 2);
            printSpaces(spaceCount);
            System.out.println("Boolean Operation: " + BOOLEAN);
            booleanSide2.accept(astVisitor, spaceCount + 2);
        } else if (TRUE != null)
            System.out.println(TRUE);
        else if (FALSE != null)
            System.out.println(FALSE);
        else if (function != null)
            function.accept(astVisitor, spaceCount + 2);
        else if (BRACOPEN != null) {
            //System.out.println(BRACOPEN);
            boolen.accept(astVisitor, spaceCount);
            //System.out.println(BRACCLOSE);
        } else {
            System.out.println("NOT Operation ..");
            if (boolen != null)
                boolen.accept(astVisitor, spaceCount + 2);
            else if (function != null)
                function.accept(astVisitor, spaceCount + 2);
            else
                var.accept(astVisitor, spaceCount + 2);
        }
        printSpaces(spaceCount);
        System.out.println("Boolean End .....");
    }

    public String getBOOLEAN() {
        return BOOLEAN;
    }

    public void setBOOLEAN(String BOOLEAN) {
        this.BOOLEAN = BOOLEAN;
    }

    public String getTRUE() {
        return TRUE;
    }

    public void setTRUE(String TRUE) {
        this.TRUE = TRUE;
    }

    public String getFALSE() {
        return FALSE;
    }

    public void setFALSE(String FALSE) {
        this.FALSE = FALSE;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public ast.model.boolen getBoolen() {
        return boolen;
    }

    public void setBoolen(ast.model.boolen boolen) {
        this.boolen = boolen;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

    public String getNOT() {
        return NOT;
    }

    public void setNOT(String NOT) {
        this.NOT = NOT;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public booleanSide getBooleanSide1() {
        return booleanSide1;
    }

    public void setBooleanSide1(booleanSide booleanSide1) {
        this.booleanSide1 = booleanSide1;
    }

    public booleanSide getBooleanSide2() {
        return booleanSide2;
    }

    public void setBooleanSide2(booleanSide booleanSide2) {
        this.booleanSide2 = booleanSide2;
    }
}
