package ast.model.Object;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Expr_object extends Node {

    String VARNAME;
    String COLUMN;
    Expression expression;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Var:" + VARNAME);
        //System.out.println(COLUMN);
        expression.accept(astVisitor, spaceCount);
    }

    public String getVARNAME() {
        return VARNAME;
    }

    public void setVARNAME(String VARNAME) {
        this.VARNAME = VARNAME;
    }

    public String getCOLUMN() {
        return COLUMN;
    }

    public void setCOLUMN(String COLUMN) {
        this.COLUMN = COLUMN;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
