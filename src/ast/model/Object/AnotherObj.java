package ast.model.Object;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class AnotherObj extends Node {

    String COMMA;
    Expr_object expr_object;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(COMMA);
        expr_object.accept(astVisitor, spaceCount);
    }

    public String getCOMMA() {
        return COMMA;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public Expr_object getExpr_object() {
        return expr_object;
    }

    public void setExpr_object(Expr_object expr_object) {
        this.expr_object = expr_object;
    }
}
