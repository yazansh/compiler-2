package ast.model.Object;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Objectt extends Node {

    String CURLYOPEN;
    Expr_object expr_object;
    ArrayList<AnotherObj> anotherObj = new ArrayList<>();
    String CURLYCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Object:");
        //System.out.println(CURLYOPEN);
        expr_object.accept(astVisitor, spaceCount + 2);
        for (int i = 0; i < anotherObj.size(); i++) {
            anotherObj.get(i).accept(astVisitor, spaceCount + 2);
        }
        //System.out.println(CURLYCLOSE);
        printSpaces(spaceCount);
        System.out.println("Object End .....");
    }

    public String getCURLYOPEN() {
        return CURLYOPEN;
    }

    public void setCURLYOPEN(String CURLYOPEN) {
        this.CURLYOPEN = CURLYOPEN;
    }

    public Expr_object getExpr_object() {
        return expr_object;
    }

    public void setExpr_object(Expr_object expr_object) {
        this.expr_object = expr_object;
    }

    public ArrayList<AnotherObj> getAnotherObj() {
        return anotherObj;
    }

    public void setAnotherObj(ArrayList<AnotherObj> anotherObj) {
        this.anotherObj = anotherObj;
    }

    public String getCURLYCLOSE() {
        return CURLYCLOSE;
    }

    public void setCURLYCLOSE(String CURLYCLOSE) {
        this.CURLYCLOSE = CURLYCLOSE;
    }
}
