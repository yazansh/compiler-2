package ast.model;

import ast.model.Function.Function;
import ast.model.Operation.MathOperation;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class booleanSide extends Node {

    Var var;
    //or
    String NUMBER;
    //or
    MathOperation mathOperation;
    //or
    Function function;


    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public MathOperation getMathOperation() {
        return mathOperation;
    }

    public void setMathOperation(MathOperation mathOperation) {
        this.mathOperation = mathOperation;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Boolean Side:");
        printSpaces(spaceCount);
        if (var != null)
            var.accept(astVisitor, spaceCount + 2);
        else if (NUMBER != null)
            System.out.println("Number: " + NUMBER);
        else if (mathOperation != null)
            mathOperation.accept(astVisitor, spaceCount + 2);
        else if (function != null)
            function.accept(astVisitor, spaceCount + 2);
        printSpaces(spaceCount);
        System.out.println("Boolean Side End .....");
    }
}
