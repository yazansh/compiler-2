package ast.model.For;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Cp_for extends Node {

    String CP_FOR;
    String EQUALL;
    String QUOTETION_OPEN;
    for_ExprFirst for_expr;
    //or
    for_ExprSecond for_exprSecond;
    //or
    Expression expression;

    String QUOTETION_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("CP_FOR:");
        //System.out.print(EQUALL);
        //System.out.print(QUOTETION_OPEN);
        if (for_expr != null)
            for_expr.accept(astVisitor, spaceCount + 2);
        else if (for_exprSecond != null)
            for_exprSecond.accept(astVisitor, spaceCount + 2);
        else
            expression.accept(astVisitor, spaceCount + 2);
        //System.out.println(QUOTETION_CLOSE);
        printSpaces(spaceCount);
        System.out.println("CP_FOR End .....");
    }

    public String getCP_FOR() {
        return CP_FOR;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public void setCP_FOR(String CP_FOR) {
        this.CP_FOR = CP_FOR;
    }

    public String getEQUALL() {
        return EQUALL;
    }

    public void setEQUALL(String EQUALL) {
        this.EQUALL = EQUALL;
    }

    public String getQUOTETION_OPEN() {
        return QUOTETION_OPEN;
    }

    public void setQUOTETION_OPEN(String QUOTETION_OPEN) {
        this.QUOTETION_OPEN = QUOTETION_OPEN;
    }

    public for_ExprFirst getFor_expr() {
        return for_expr;
    }

    public void setFor_expr(for_ExprFirst for_expr) {
        this.for_expr = for_expr;
    }

    public for_ExprSecond getFor_exprSecond() {
        return for_exprSecond;
    }

    public void setFor_exprSecond(for_ExprSecond for_exprSecond) {
        this.for_exprSecond = for_exprSecond;
    }

    public String getQUOTETION_CLOSE() {
        return QUOTETION_CLOSE;
    }

    public void setQUOTETION_CLOSE(String QUOTETION_CLOSE) {
        this.QUOTETION_CLOSE = QUOTETION_CLOSE;
    }
}
