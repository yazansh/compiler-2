package ast.model.For;

import ast.model.Expression.One_line_exp;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class BeforIN extends Node {

    Var var;
    //or
    One_line_exp one_line_exp;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        if (var != null)
            var.accept(astVisitor,spaceCount);
        if (one_line_exp != null)
            one_line_exp.accept(astVisitor,spaceCount);
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }
}
