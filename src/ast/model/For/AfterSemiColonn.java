package ast.model.For;

import ast.model.Expression.One_line_exp;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class AfterSemiColonn extends Node {

    String SEMICOLONN;
    Var var;
    //or
    One_line_exp one_line_exp;

    String EQUAL;

    String INDEX;
    //or
    Var varSecond;
    //or
    String NUMBER;
    //or
    //testing for 2 variable>>>
    One_line_exp one_line_expSecond;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(SEMICOLONN);
        if (var != null)
            var.accept(astVisitor,spaceCount);
        else
            one_line_exp.accept(astVisitor,spaceCount);
        //System.out.print(EQUAL);
        if (INDEX != null);
            //System.out.println(INDEX);
        if (varSecond != null);
            //varSecond.accept(astVisitor,spaceCount);
        if (NUMBER != null);
            //System.out.println(NUMBER);
        if (one_line_expSecond != null);
            //one_line_expSecond.accept(astVisitor,spaceCount);
    }

    public String getEQUAL() {
        return EQUAL;
    }

    public void setEQUAL(String EQUAL) {
        this.EQUAL = EQUAL;
    }

    public String getSEMICOLONN() {
        return SEMICOLONN;
    }

    public void setSEMICOLONN(String SEMICOLONN) {
        this.SEMICOLONN = SEMICOLONN;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public String getINDEX() {
        return INDEX;
    }

    public void setINDEX(String INDEX) {
        this.INDEX = INDEX;
    }

    public Var getVarSecond() {
        return varSecond;
    }

    public void setVarSecond(Var varSecond) {
        this.varSecond = varSecond;
    }

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public One_line_exp getOne_line_expSecond() {
        return one_line_expSecond;
    }

    public void setOne_line_expSecond(One_line_exp one_line_expSecond) {
        this.one_line_expSecond = one_line_expSecond;
    }
}
