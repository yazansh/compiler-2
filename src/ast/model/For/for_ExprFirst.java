package ast.model.For;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class for_ExprFirst extends Node {

    BeforIN beforIN;
    String IN;
    AfterIN afterIN;

    //afterSemiColonn?
    AfterSemiColonn afterSemiColonn;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        beforIN.accept(astVisitor,spaceCount);
        printSpaces(spaceCount);
        System.out.println("IN ...");
        afterIN.accept(astVisitor,spaceCount);
        if (afterSemiColonn != null)
            afterSemiColonn.accept(astVisitor,spaceCount);
    }

    public BeforIN getBeforIN() {
        return beforIN;
    }

    public void setBeforIN(BeforIN beforIN) {
        this.beforIN = beforIN;
    }

    public String getIN() {
        return IN;
    }

    public void setIN(String IN) {
        this.IN = IN;
    }

    public AfterIN getAfterIN() {
        return afterIN;
    }

    public void setAfterIN(AfterIN afterIN) {
        this.afterIN = afterIN;
    }

    public AfterSemiColonn getAfterSemiColonn() {
        return afterSemiColonn;
    }

    public void setAfterSemiColonn(AfterSemiColonn afterSemiColonn) {
        this.afterSemiColonn = afterSemiColonn;
    }
}
