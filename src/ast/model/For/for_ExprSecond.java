package ast.model.For;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class for_ExprSecond extends Node {

    BeforIN beforIN;
    String COMMA;
    BeforIN beforINSecond;
    String IN;
    AfterIN afterIN;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        beforIN.accept(astVisitor,spaceCount);
        //System.out.println(COMMA);
        beforINSecond.accept(astVisitor,spaceCount);
        printSpaces(spaceCount);
        System.out.println("IN ...");
        afterIN.accept(astVisitor,spaceCount);
    }

    public BeforIN getBeforIN() {
        return beforIN;
    }

    public void setBeforIN(BeforIN beforIN) {
        this.beforIN = beforIN;
    }

    public String getCOMMA() {
        return COMMA;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public BeforIN getBeforINSecond() {
        return beforINSecond;
    }

    public void setBeforINSecond(BeforIN beforINSecond) {
        this.beforINSecond = beforINSecond;
    }

    public String getIN() {
        return IN;
    }

    public void setIN(String IN) {
        this.IN = IN;
    }

    public AfterIN getAfterIN() {
        return afterIN;
    }

    public void setAfterIN(AfterIN afterIN) {
        this.afterIN = afterIN;
    }
}
