package ast.model.Array;

import ast.model.Function.Function;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class ArrayIndex extends Node {

    String ARRAYOPEN;
    String NUMBER;
    //or
    Var var;
    //or
    Function function;
    String ARRAYCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor , int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        //System.out.print(ARRAYOPEN);
        if (NUMBER != null)
            System.out.println(NUMBER);
        else if (var != null) {
            var.accept(astVisitor,spaceCount);
        } else {
            function.accept(astVisitor,spaceCount);
        }
        //System.out.println(ARRAYCLOSE);
    }

    public String getARRAYOPEN() {
        return ARRAYOPEN;
    }

    public void setARRAYOPEN(String ARRAYOPEN) {
        this.ARRAYOPEN = ARRAYOPEN;
    }

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }


    public String getARRAYCLOSE() {
        return ARRAYCLOSE;
    }

    public void setARRAYCLOSE(String ARRAYCLOSE) {
        this.ARRAYCLOSE = ARRAYCLOSE;
    }

}
