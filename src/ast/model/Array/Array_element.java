package ast.model.Array;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Array_element extends Node {

    String VARNAME;
    ArrayList<ArrayElementExp> arrayElementExp = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor , int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Array Element:");
        printSpaces(spaceCount);
        System.out.println(VARNAME);
        for (int i = 0; i < arrayElementExp.size(); i++) {
            arrayElementExp.get(i).accept(astVisitor,spaceCount);
        }
        printSpaces(spaceCount);
        System.out.println("Array Element End .....");
    }

    public String getVARNAME() {
        return VARNAME;
    }

    public void setVARNAME(String VARNAME) {
        this.VARNAME = VARNAME;
    }

    public ArrayList<ArrayElementExp> getArrayElementExp() {
        return arrayElementExp;
    }

    public void setArrayElementExp(ArrayList<ArrayElementExp> arrayElementExp) {
        this.arrayElementExp = arrayElementExp;
    }
}
