package ast.model.Array;

import ast.model.Function.Function_expr;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class ArrayElementExp extends Node {

    ArrayList<Function_expr> function_expr = new ArrayList<>();
    //arrayIndex+
    ArrayList<ArrayIndex> arrayIndex = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        for (int i = 0; i < function_expr.size(); i++) {
            function_expr.get(i).accept(astVisitor, spaceCount + 2);
        }
        for (int i = 0; i < arrayIndex.size(); i++) {
            arrayIndex.get(i).accept(astVisitor, spaceCount + 2);
        }
    }

    public ArrayList<Function_expr> getFunction_expr() {
        return function_expr;
    }

    public void setFunction_expr(ArrayList<Function_expr> function_expr) {
        this.function_expr = function_expr;
    }

    public ArrayList<ArrayIndex> getArrayIndex() {
        return arrayIndex;
    }

    public void setArrayIndex(ArrayList<ArrayIndex> arrayIndex) {
        this.arrayIndex = arrayIndex;
    }
}
