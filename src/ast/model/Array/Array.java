package ast.model.Array;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Array extends Node {

    String ARRAYOPEN;
    Expression expression;
    ArrayList<AnotherArrayElement> anotherArrayElement = new ArrayList<>();
    String ARRAYCLOSE;

    //or
    String BRACOPEN;
    Array array;
    String BRACCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Array:");
        if (ARRAYOPEN != null) {
            //System.out.print(ARRAYOPEN);
            expression.accept(astVisitor, spaceCount + 2);
            for (int i = 0; i < anotherArrayElement.size(); i++) {
                anotherArrayElement.get(i).accept(astVisitor, spaceCount + 2);
            }
            //System.out.println(ARRAYCLOSE);
        } else {
            //System.out.println(BRACOPEN);
            array.accept(astVisitor, spaceCount);
            //System.out.println(BRACCLOSE);
        }
        printSpaces(spaceCount);
        System.out.println("Array End .....");
    }

    public String getARRAYOPEN() {
        return ARRAYOPEN;
    }

    public void setARRAYOPEN(String ARRAYOPEN) {
        this.ARRAYOPEN = ARRAYOPEN;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public ArrayList<AnotherArrayElement> getAnotherArrayElement() {
        return anotherArrayElement;
    }

    public void setAnotherArrayElement(ArrayList<AnotherArrayElement> anotherArrayElement) {
        this.anotherArrayElement = anotherArrayElement;
    }

    public String getARRAYCLOSE() {
        return ARRAYCLOSE;
    }

    public void setARRAYCLOSE(String ARRAYCLOSE) {
        this.ARRAYCLOSE = ARRAYCLOSE;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public Array getArray() {
        return array;
    }

    public void setArray(Array array) {
        this.array = array;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

}
