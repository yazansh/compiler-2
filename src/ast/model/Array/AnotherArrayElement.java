package ast.model.Array;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class AnotherArrayElement extends Node {

    String COMMA;
    Expression expression;

    public String getCOMMA() {
        return COMMA;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.print(COMMA);
        expression.accept(astVisitor,spaceCount);
    }
}
