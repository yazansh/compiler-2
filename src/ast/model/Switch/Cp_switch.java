package ast.model.Switch;

import ast.model.Expression.One_line_exp;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Cp_switch extends Node {

    String CP_SWITCH;
    String EQUALL;
    String QUOTETION_OPEN;
    Var var;
    //or
    One_line_exp one_line_exp;
    String QUOTETION_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("CP_SWITCH:");
        //System.out.print(EQUALL);
        //System.out.print(QUOTETION_OPEN);
        if (var != null)
            var.accept(astVisitor, spaceCount + 2);
        else
            one_line_exp.accept(astVisitor, spaceCount + 2);
        //System.out.println(QUOTETION_CLOSE);
        printSpaces(spaceCount);
        System.out.println("CP_SWITCH End .....");
    }

    public String getCP_SWITCH() {
        return CP_SWITCH;
    }

    public void setCP_SWITCH(String CP_SWITCH) {
        this.CP_SWITCH = CP_SWITCH;
    }

    public String getEQUALL() {
        return EQUALL;
    }

    public void setEQUALL(String EQUALL) {
        this.EQUALL = EQUALL;
    }

    public String getQUOTETION_OPEN() {
        return QUOTETION_OPEN;
    }

    public void setQUOTETION_OPEN(String QUOTETION_OPEN) {
        this.QUOTETION_OPEN = QUOTETION_OPEN;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public String getQUOTETION_CLOSE() {
        return QUOTETION_CLOSE;
    }

    public void setQUOTETION_CLOSE(String QUOTETION_CLOSE) {
        this.QUOTETION_CLOSE = QUOTETION_CLOSE;
    }
}
