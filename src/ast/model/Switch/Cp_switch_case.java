package ast.model.Switch;

import ast.model.Expression.One_line_exp;
import ast.model.Operation.logic;
import ast.model.Var.Constt;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Cp_switch_case extends Node {

    String CP_SWITCH_CASE;
    String EQUALL;
    String QUOTETION_OPEN;
    Constt constt;
    //or
    Var var;
    //or
    One_line_exp one_line_exp;
    //or
    logic logic;

    String QUOTETION_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("CP_SWITCH_CASE:");
        //System.out.print(EQUALL);
        //System.out.print(QUOTETION_OPEN);
        if (constt != null)
            constt.accept(astVisitor, spaceCount + 2);
        else if (var != null)
            var.accept(astVisitor, spaceCount + 2);
        else if (one_line_exp != null)
            one_line_exp.accept(astVisitor, spaceCount + 2);
        else logic.accept(astVisitor, spaceCount + 2);
        //System.out.println(QUOTETION_CLOSE);
        printSpaces(spaceCount);
        System.out.println("CP_SWITCH_CASE end ....");
    }

    public String getCP_SWITCH_CASE() {
        return CP_SWITCH_CASE;
    }

    public void setCP_SWITCH_CASE(String CP_SWITCH_CASE) {
        this.CP_SWITCH_CASE = CP_SWITCH_CASE;
    }

    public String getEQUALL() {
        return EQUALL;
    }

    public void setEQUALL(String EQUALL) {
        this.EQUALL = EQUALL;
    }

    public String getQUOTETION_OPEN() {
        return QUOTETION_OPEN;
    }

    public void setQUOTETION_OPEN(String QUOTETION_OPEN) {
        this.QUOTETION_OPEN = QUOTETION_OPEN;
    }

    public Constt getConstt() {
        return constt;
    }

    public void setConstt(Constt constt) {
        this.constt = constt;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public ast.model.Operation.logic getLogic() {
        return logic;
    }

    public void setLogic(ast.model.Operation.logic logic) {
        this.logic = logic;
    }

    public String getQUOTETION_CLOSE() {
        return QUOTETION_CLOSE;
    }

    public void setQUOTETION_CLOSE(String QUOTETION_CLOSE) {
        this.QUOTETION_CLOSE = QUOTETION_CLOSE;
    }
}
