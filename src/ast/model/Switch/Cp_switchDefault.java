package ast.model.Switch;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Cp_switchDefault extends Node {

    String CP_SWITCHDEFAULT;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("CP_SWITCHDEFAULT .....");
    }

    public String getCP_SWITCHDEFAULT() {
        return CP_SWITCHDEFAULT;
    }

    public void setCP_SWITCHDEFAULT(String CP_SWITCHDEFAULT) {
        this.CP_SWITCHDEFAULT = CP_SWITCHDEFAULT;
    }
}
