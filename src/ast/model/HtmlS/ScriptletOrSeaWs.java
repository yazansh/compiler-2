package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class ScriptletOrSeaWs extends Node {

    private String SCRIPTLET;
    private String SEA_WS;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if(SCRIPTLET!=null)
        ;//System.out.println(SCRIPTLET);
    }

    public String getSCRIPTLET() {
        return SCRIPTLET;
    }

    public void setSCRIPTLET(String SCRIPTLET) {
        this.SCRIPTLET = SCRIPTLET;
    }

    public String getSEA_WS() {
        return SEA_WS;
    }

    public void setSEA_WS(String SEA_WS) {
        this.SEA_WS = SEA_WS;
    }
}
