package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class HtmlChardata extends Node {

    String HTML_TEXT;
    //or
    String SEA_WS;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (HTML_TEXT != null)
            ;//System.out.println(HTML_TEXT);
    }

    public String getHTML_TEXT() {
        return HTML_TEXT;
    }

    public void setHTML_TEXT(String HTML_TEXT) {
        this.HTML_TEXT = HTML_TEXT;
    }

    public String getSEA_WS() {
        return SEA_WS;
    }

    public void setSEA_WS(String SEA_WS) {
        this.SEA_WS = SEA_WS;
    }
}
