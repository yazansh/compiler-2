package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class HtmlMisc extends Node {
    private HtmlComment htmlComment;

    String SEA_WS;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (htmlComment != null)
            htmlComment.accept(astVisitor,spaceCount);
    }

    public String getSEA_WS() {
        return SEA_WS;
    }

    public void setSEA_WS(String SEA_WS) {
        this.SEA_WS = SEA_WS;
    }

    public HtmlComment getHtmlComment() {
        return htmlComment;
    }

    public void setHtmlComment(HtmlComment htmlComment) {
        this.htmlComment = htmlComment;
    }
}
