package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Script extends Node {

    String SCRIPT_OPEN;
    String SCRIPT_BODY;
    //or
    String SCRIPT_SHORT_BODY;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(SCRIPT_OPEN);
        if (SCRIPT_BODY != null)
            ;//System.out.println(SCRIPT_BODY);
        else
            ;//System.out.println(SCRIPT_SHORT_BODY);
    }

    public void setSCRIPT_OPEN(String SCRIPT_OPEN) {
        this.SCRIPT_OPEN = SCRIPT_OPEN;
    }

    public void setSCRIPT_BODY(String SCRIPT_BODY) {
        this.SCRIPT_BODY = SCRIPT_BODY;
    }

    public void setSCRIPT_SHORT_BODY(String SCRIPT_SHORT_BODY) {
        this.SCRIPT_SHORT_BODY = SCRIPT_SHORT_BODY;
    }

    public String getSCRIPT_OPEN() {
        return SCRIPT_OPEN;
    }

    public String getSCRIPT_BODY() {
        return SCRIPT_BODY;
    }

    public String getSCRIPT_SHORT_BODY() {
        return SCRIPT_SHORT_BODY;
    }
}
