package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Style extends Node {

    String STYLE_OPEN;
    String STYLE_BODY;
    String STYLE_SHORT_BODY;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(STYLE_OPEN);
        if(STYLE_BODY!=null)
            ;//System.out.println(STYLE_BODY);
        else
            ;//System.out.println(STYLE_SHORT_BODY);
    }

    public String getSTYLE_OPEN() {
        return STYLE_OPEN;
    }

    public String getSTYLE_BODY() {
        return STYLE_BODY;
    }

    public String getSTYLE_SHORT_BODY() {
        return STYLE_SHORT_BODY;
    }

    public void setSTYLE_OPEN(String STYLE_OPEN) {
        this.STYLE_OPEN = STYLE_OPEN;
    }

    public void setSTYLE_BODY(String STYLE_BODY) {
        this.STYLE_BODY = STYLE_BODY;
    }

    public void setSTYLE_SHORT_BODY(String STYLE_SHORT_BODY) {
        this.STYLE_SHORT_BODY = STYLE_SHORT_BODY;
    }
}
