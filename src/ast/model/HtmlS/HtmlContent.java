package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class HtmlContent extends Node {

    //htmlChardata?
    HtmlChardata htmlChardata;
    ArrayList<HtmlStructure> htmlStructure = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (htmlChardata != null)
            htmlChardata.accept(astVisitor, spaceCount);
        for (int i = 0; i < htmlStructure.size(); i++) {
            htmlStructure.get(i).accept(astVisitor, spaceCount);
        }
    }

    public HtmlChardata getHtmlChardata() {
        return htmlChardata;
    }

    public void setHtmlChardata(HtmlChardata htmlChardata) {
        this.htmlChardata = htmlChardata;
    }

    public ArrayList<HtmlStructure> getHtmlStructure() {
        return htmlStructure;
    }

    public void setHtmlStructure(ArrayList<HtmlStructure> htmlStructure) {
        this.htmlStructure = htmlStructure;
    }
}
