package ast.model.HtmlS;

import ast.model.Binding.Binding;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class HtmlStructure extends Node {

    HtmlElement htmlElement;
    //or
    String CDATA;
    //or
    HtmlComment htmlComment;
    //or
    Binding binding;

    HtmlChardata htmlChardata;//htmlChardata?

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (htmlElement != null)
            htmlElement.accept(astVisitor,spaceCount);
        if (htmlComment != null)
            htmlComment.accept(astVisitor,spaceCount);
        if (binding != null)
            binding.accept(astVisitor,spaceCount);
        if (htmlChardata != null)
            htmlChardata.accept(astVisitor,spaceCount);
    }

    public HtmlChardata getHtmlChardata() {
        return htmlChardata;
    }

    public void setHtmlChardata(HtmlChardata htmlChardata) {
        this.htmlChardata = htmlChardata;
    }

    public HtmlElement getHtmlElement() {
        return htmlElement;
    }

    public void setHtmlElement(HtmlElement htmlElement) {
        this.htmlElement = htmlElement;
    }

    public String getCDATA() {
        return CDATA;
    }

    public void setCDATA(String CDATA) {
        this.CDATA = CDATA;
    }

    public HtmlComment getHtmlComment() {
        return htmlComment;
    }

    public void setHtmlComment(HtmlComment htmlComment) {
        this.htmlComment = htmlComment;
    }

    public Binding getBinding() {
        return binding;
    }

    public void setBinding(Binding binding) {
        this.binding = binding;
    }
}
