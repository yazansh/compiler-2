package ast.model.HtmlS;

import ast.model.Attribue.ElementAttribute;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class HtmlElement extends Node {

    private String TAG_OPEN;
    private String TAG_NAME;
    private ArrayList<ElementAttribute> elementAttributes = new ArrayList<>();
    ContainerHtmlContent containerHtmlContent;
    private String TAG_CLOSE;
    //or
    private String TAG_SLASH_CLOSE;
    private String SCRIPTLET;
    private Script script;
    private Style style;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        if(TAG_OPEN!=null) {
            //System.out.println(TAG_OPEN);
            //System.out.println(TAG_NAME);
            for (int i = 0; i < elementAttributes.size(); i++) {
                elementAttributes.get(i).accept(astVisitor,spaceCount);
            }
            if (TAG_CLOSE != null) {
                //System.out.println(TAG_CLOSE);
                if (containerHtmlContent != null)
                    containerHtmlContent.accept(astVisitor,spaceCount);
            } else
                ;//System.out.println(TAG_SLASH_CLOSE);
        }
        if (script != null)
            script.accept(astVisitor,spaceCount);
        if (style != null)
            style.accept(astVisitor,spaceCount);
    }

    public ContainerHtmlContent getContainerHtmlContent() {
        return containerHtmlContent;
    }

    public void setContainerHtmlContent(ContainerHtmlContent containerHtmlContent) {
        this.containerHtmlContent = containerHtmlContent;
    }

    public String getSCRIPTLET() {
        return SCRIPTLET;
    }

    public void setSCRIPTLET(String SCRIPTLET) {
        this.SCRIPTLET = SCRIPTLET;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getTAG_OPEN() {
        return TAG_OPEN;
    }

    public void setTAG_OPEN(String TAG_OPEN) {
        this.TAG_OPEN = TAG_OPEN;
    }

    public String getTAG_NAME() {
        return TAG_NAME;
    }

    public void setTAG_NAME(String TAG_NAME) {
        this.TAG_NAME = TAG_NAME;
    }

    public ArrayList<ElementAttribute> getElementAttributes() {
        return elementAttributes;
    }

    public void setElementAttributes(ArrayList<ElementAttribute> elementAttributes) {
        this.elementAttributes = elementAttributes;
    }

    public String getTAG_CLOSE() {
        return TAG_CLOSE;
    }

    public void setTAG_CLOSE(String TAG_CLOSE) {
        this.TAG_CLOSE = TAG_CLOSE;
    }

    public String getTAG_SLASH_CLOSE() {
        return TAG_SLASH_CLOSE;
    }

    public void setTAG_SLASH_CLOSE(String TAG_SLASH_CLOSE) {
        this.TAG_SLASH_CLOSE = TAG_SLASH_CLOSE;
    }
}
