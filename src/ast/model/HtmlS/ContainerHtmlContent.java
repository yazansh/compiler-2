package ast.model.HtmlS;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class ContainerHtmlContent extends Node {
    private HtmlContent htmlContent;
    private String TAG_OPEN;
    private String TAG_SLASH;
    private String TAG_NAME;
    private String TAG_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        htmlContent.accept(astVisitor,spaceCount);
        System.out.print(TAG_OPEN);
        System.out.print(TAG_SLASH);
        System.out.print(TAG_NAME);
        System.out.println(TAG_CLOSE);
    }

    public HtmlContent getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(HtmlContent htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getTAG_OPEN() {
        return TAG_OPEN;
    }

    public void setTAG_OPEN(String TAG_OPEN) {
        this.TAG_OPEN = TAG_OPEN;
    }

    public String getTAG_SLASH() {
        return TAG_SLASH;
    }

    public void setTAG_SLASH(String TAG_SLASH) {
        this.TAG_SLASH = TAG_SLASH;
    }

    public String getTAG_NAME() {
        return TAG_NAME;
    }

    public void setTAG_NAME(String TAG_NAME) {
        this.TAG_NAME = TAG_NAME;
    }

    public String getTAG_CLOSE() {
        return TAG_CLOSE;
    }

    public void setTAG_CLOSE(String TAG_CLOSE) {
        this.TAG_CLOSE = TAG_CLOSE;
    }
}
