package ast.model.HtmlS;

import ast.model.Attribue.AttributeValue;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class HtmlAttribute extends Node {

    String TAG_NAME;
    //attributeValue?
    AttributeValue attributeValue;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        //printSpaces(spaceCount);
        //System.out.println(TAG_NAME);
        if (attributeValue != null)
            attributeValue.accept(astVisitor,spaceCount);
    }

    public String getTAG_NAME() {
        return TAG_NAME;
    }

    public void setTAG_NAME(String TAG_NAME) {
        this.TAG_NAME = TAG_NAME;
    }

    public AttributeValue getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(AttributeValue attributeValue) {
        this.attributeValue = attributeValue;
    }
}
