package ast.model.Operation;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class MathOperation extends Node {

    OperationSide operationSide;
    ArrayList<OperationSideWithPlusMinus> operationSideWithPlusMinuses = new ArrayList<>();

    //or
    OperationElement operationElement;
    String PLUSPLUS;
    //or
    String MINUSMINUS;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Math Operation:");
        if (operationSide != null) {
            operationSide.accept(astVisitor, spaceCount + 2);
            for (int i = 0; i < operationSideWithPlusMinuses.size(); i++)
                operationSideWithPlusMinuses.get(i).accept(astVisitor, spaceCount + 2);
        } else {
            operationElement.accept(astVisitor, spaceCount + 2);
            if (PLUSPLUS != null) {
                printSpaces(spaceCount);
                System.out.println(PLUSPLUS);
            } else {
                printSpaces(spaceCount);
                System.out.println(MINUSMINUS);
            }
        }
        printSpaces(spaceCount);
        System.out.println("Math Operation End ......");
    }

    public OperationSide getOperationSide() {
        return operationSide;
    }

    public void setOperationSide(OperationSide operationSide) {
        this.operationSide = operationSide;
    }

    public ArrayList<OperationSideWithPlusMinus> getOperationSideWithPlusMinuses() {
        return operationSideWithPlusMinuses;
    }

    public void setOperationSideWithPlusMinuses(ArrayList<OperationSideWithPlusMinus> operationSideWithPlusMinuses) {
        this.operationSideWithPlusMinuses = operationSideWithPlusMinuses;
    }

    public OperationElement getOperationElement() {
        return operationElement;
    }

    public void setOperationElement(OperationElement operationElement) {
        this.operationElement = operationElement;
    }

    public String getPLUSPLUS() {
        return PLUSPLUS;
    }

    public void setPLUSPLUS(String PLUSPLUS) {
        this.PLUSPLUS = PLUSPLUS;
    }

    public String getMINUSMINUS() {
        return MINUSMINUS;
    }

    public void setMINUSMINUS(String MINUSMINUS) {
        this.MINUSMINUS = MINUSMINUS;
    }
}
