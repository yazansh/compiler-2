package ast.model.Operation;

import ast.model.Var.Var;
import ast.model.boolen;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class logicSide extends Node {

    String LOGIC;
    boolen boolen;
    //or
    Var var;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Logic Operation: " + LOGIC);
        if (boolen != null)
            boolen.accept(astVisitor, spaceCount);
        else
            var.accept(astVisitor, spaceCount);
    }

    public String getLOGIC() {
        return LOGIC;
    }

    public void setLOGIC(String LOGIC) {
        this.LOGIC = LOGIC;
    }

    public ast.model.boolen getBoolen() {
        return boolen;
    }

    public void setBoolen(ast.model.boolen boolen) {
        this.boolen = boolen;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }
}
