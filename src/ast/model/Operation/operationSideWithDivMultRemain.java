package ast.model.Operation;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class operationSideWithDivMultRemain  extends Node {

    String DIV;
    //or
    String MULT;
    //or
    String REMAIN;

    OperationSide operationSide;

    public String getDIV() {
        return DIV;
    }

    public void setDIV(String DIV) {
        this.DIV = DIV;
    }

    public String getMULT() {
        return MULT;
    }

    public void setMULT(String MULT) {
        this.MULT = MULT;
    }

    public String getREMAIN() {
        return REMAIN;
    }

    public void setREMAIN(String REMAIN) {
        this.REMAIN = REMAIN;
    }

    public OperationSide getOperationSide() {
        return operationSide;
    }

    public void setOperationSide(OperationSide operationSide) {
        this.operationSide = operationSide;
    }

    @Override
        public void accept(ASTVisitor astVisitor,int spaceCount){
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.print("Operation: ");
        if(DIV!= null){
            System.out.println(DIV);
        }else if (MULT!=null){
            System.out.println(MULT);
        }else {
            System.out.println(REMAIN);
        }
        operationSide.accept(astVisitor,spaceCount+2);
    }
}
