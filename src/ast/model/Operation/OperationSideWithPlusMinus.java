package ast.model.Operation;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class OperationSideWithPlusMinus extends Node {

    String PLUS;
    //or
    String MINUS;

    OperationSide operationSide;

    public String getPLUS() {
        return PLUS;
    }

    public void setPLUS(String PLUS) {
        this.PLUS = PLUS;
    }

    public String getMINUS() {
        return MINUS;
    }

    public void setMINUS(String MINUS) {
        this.MINUS = MINUS;
    }

    public OperationSide getOperationSide() {
        return operationSide;
    }

    public void setOperationSide(OperationSide operationSide) {
        this.operationSide = operationSide;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.print("Operation: ");
        if (PLUS != null)
            System.out.println(PLUS);
        else
            System.out.println(MINUS);

        operationSide.accept(astVisitor, spaceCount);
    }
}
