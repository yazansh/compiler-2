package ast.model.Operation;

import ast.model.Function.Function;
import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class OperationElement extends Node {

    String NUMBER;
    //or
    Var var;
    //or
    Function function;

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (var != null)
            var.accept(astVisitor, spaceCount);
        else if (function != null)
            function.accept(astVisitor, spaceCount);
        else {
            printSpaces(spaceCount);
            System.out.println(NUMBER);
        }
    }
}
