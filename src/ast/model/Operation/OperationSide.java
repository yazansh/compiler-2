package ast.model.Operation;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class OperationSide extends Node {

    OperationElement operationElement;
    //or
    String BRACOPEN;
    MathOperation mathOperation;
    String BRACCLOSE;
    ArrayList<operationSideWithDivMultRemain> operationSideWithDivMultRemain = new ArrayList<>();

    //or
    OperationSide operationSide;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Operation Side: ");
        if (operationElement != null) {
            operationElement.accept(astVisitor, spaceCount + 2);
            for (int i = 0; i < operationSideWithDivMultRemain.size(); i++) {
                operationSideWithDivMultRemain.get(i).accept(astVisitor, spaceCount + 2);
            }
        } else {
            //System.out.println(BRACOPEN);
            if (mathOperation != null) {
                mathOperation.accept(astVisitor, spaceCount + 2);
                //System.out.println(BRACCLOSE);
                for (int i = 0; i < operationSideWithDivMultRemain.size(); i++)
                    operationSideWithDivMultRemain.get(i).accept(astVisitor, spaceCount + 2);
            } else {
                operationSide.accept(astVisitor, spaceCount + 2);
                //System.out.println(BRACCLOSE);
            }
        }
        printSpaces(spaceCount);
        System.out.println("Operation Side End .....");
    }

    public OperationElement getOperationElement() {
        return operationElement;
    }

    public void setOperationElement(OperationElement operationElement) {
        this.operationElement = operationElement;
    }

    public ArrayList<ast.model.Operation.operationSideWithDivMultRemain> getOperationSideWithDivMultRemain() {
        return operationSideWithDivMultRemain;
    }

    public void setOperationSideWithDivMultRemain(ArrayList<ast.model.Operation.operationSideWithDivMultRemain> operationSideWithDivMultRemain) {
        this.operationSideWithDivMultRemain = operationSideWithDivMultRemain;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public MathOperation getMathOperation() {
        return mathOperation;
    }

    public void setMathOperation(MathOperation mathOperation) {
        this.mathOperation = mathOperation;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

    public OperationSide getOperationSide() {
        return operationSide;
    }

    public void setOperationSide(OperationSide operationSide) {
        this.operationSide = operationSide;
    }

}
