package ast.model.Operation;

import ast.model.Var.Var;
import ast.model.boolen;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class logic extends Node {

    boolen boolen;
    //or
    Var var;
    ArrayList<logicSide> logicSides = new ArrayList<>();

    //or
    String NOT;
    String BRACOPEN;
    logic logic;
    String BRACCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Logic:");
        if (boolen != null) {
            boolen.accept(astVisitor, spaceCount + 2);
            for (int i = 0; i < logicSides.size(); i++) {
                logicSides.get(i).accept(astVisitor, spaceCount + 2);
            }
        } else if (var != null) {
            var.accept(astVisitor, spaceCount + 2);
            for (int i = 0; i < logicSides.size(); i++) {
                logicSides.get(i).accept(astVisitor, spaceCount + 2);
            }
        } else {
            if (NOT != null)
                ;//System.out.println(NOT);
            //System.out.println(BRACOPEN);
            logic.accept(astVisitor, spaceCount + 2);
            //System.out.println(BRACCLOSE);
        }
        printSpaces(spaceCount);
        System.out.println("Logic End .....");
    }

    public ast.model.boolen getBoolen() {
        return boolen;
    }

    public void setBoolen(ast.model.boolen boolen) {
        this.boolen = boolen;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public ArrayList<logicSide> getLogicSides() {
        return logicSides;
    }

    public void setLogicSides(ArrayList<logicSide> logicSides) {
        this.logicSides = logicSides;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public ast.model.Operation.logic getLogic() {
        return logic;
    }

    public void setLogic(ast.model.Operation.logic logic) {
        this.logic = logic;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

    public String getNOT() {
        return NOT;
    }

    public void setNOT(String NOT) {
        this.NOT = NOT;
    }
}
