package ast.model.Event;

import ast.model.Function.Function;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Event extends Node {

    String EVENT_NAME;
    String EQUALL;
    String QUOTETION_OPEN;
    Function function;
    String QUOTETION_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Event:");
        printSpaces(spaceCount);
        System.out.print(EVENT_NAME);
        //System.out.print(EQUALL);
        //System.out.print(QUOTETION_OPEN);
        function.accept(astVisitor,spaceCount);
        //System.out.println(QUOTETION_CLOSE);
        printSpaces(spaceCount);
        System.out.println("Event End .....");
    }

    public String getEVENT_NAME() {
        return EVENT_NAME;
    }

    public void setEVENT_NAME(String EVENT_NAME) {
        this.EVENT_NAME = EVENT_NAME;
    }

    public String getEQUALL() {
        return EQUALL;
    }

    public void setEQUALL(String EQUALL) {
        this.EQUALL = EQUALL;
    }

    public String getQUOTETION_OPEN() {
        return QUOTETION_OPEN;
    }

    public void setQUOTETION_OPEN(String QUOTETION_OPEN) {
        this.QUOTETION_OPEN = QUOTETION_OPEN;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public String getQUOTETION_CLOSE() {
        return QUOTETION_CLOSE;
    }

    public void setQUOTETION_CLOSE(String QUOTETION_CLOSE) {
        this.QUOTETION_CLOSE = QUOTETION_CLOSE;
    }
}
