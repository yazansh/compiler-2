package ast.model.Var;

import ast.model.Function.Function;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class ConstOperation extends Node {
    String CHAR;
    //or
    Var var;
    //or
    Function function;

    ArrayList<ConstSide> constSide = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        if (CHAR != null)
            System.out.println(CHAR);
        else if (var != null)
            var.accept(astVisitor,spaceCount);
        else
            function.accept(astVisitor,spaceCount);
        for (int i = 0; i < constSide.size(); i++)
            constSide.get(i).accept(astVisitor,spaceCount);
    }

    public String getCHAR() {
        return CHAR;
    }

    public void setCHAR(String CHAR) {
        this.CHAR = CHAR;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public ArrayList<ConstSide> getConstSide() {
        return constSide;
    }

    public void setConstSide(ArrayList<ConstSide> constSide) {
        this.constSide = constSide;
    }
}
