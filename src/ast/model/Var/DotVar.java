package ast.model.Var;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class DotVar extends Node {

    String DOT;
    Var var;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("DOT ......");
        var.accept(astVisitor,spaceCount);
    }

    public String getDOT() {
        return DOT;
    }

    public void setDOT(String DOT) {
        this.DOT = DOT;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }
}
