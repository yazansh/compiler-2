package ast.model.Var;

import ast.model.Array.Array_element;
import ast.model.If.One_line_condition;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Simple_var extends Node {

    String VARNAME;
    //or
    Array_element array_element;
    //or
    One_line_condition one_line_condition;
    //or
    String BRACOPEN;
    Simple_var simple_var;
    String BRACCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        if (VARNAME != null)
            System.out.println("Name: " + VARNAME);
        else if (array_element != null)
            array_element.accept(astVisitor, spaceCount);
        else if (one_line_condition != null)
            one_line_condition.accept(astVisitor, spaceCount);
        else {
            //System.out.println(BRACOPEN);
            simple_var.accept(astVisitor, spaceCount);
            //System.out.println(BRACCLOSE);
        }
    }

    public String getVARNAME() {
        return VARNAME;
    }

    public void setVARNAME(String VARNAME) {
        this.VARNAME = VARNAME;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public Simple_var getSimple_var() {
        return simple_var;
    }

    public void setSimple_var(Simple_var simple_var) {
        this.simple_var = simple_var;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

    public Array_element getArray_element() {
        return array_element;
    }

    public void setArray_element(Array_element array_element) {
        this.array_element = array_element;
    }

    public One_line_condition getOne_line_condition() {
        return one_line_condition;
    }

    public void setOne_line_condition(One_line_condition one_line_condition) {
        this.one_line_condition = one_line_condition;
    }
}
