package ast.model.Var;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class ConstSide extends Node {
    String PLUS;
    //or
    String MINUS;

    ConstOperation constOperation;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        if (PLUS != null)
            System.out.println("Operation: " + PLUS);
        else
            System.out.println("Operation: " + MINUS);
        constOperation.accept(astVisitor, spaceCount);
    }

    public String getPLUS() {
        return PLUS;
    }

    public void setPLUS(String PLUS) {
        this.PLUS = PLUS;
    }

    public String getMINUS() {
        return MINUS;
    }

    public void setMINUS(String MINUS) {
        this.MINUS = MINUS;
    }

    public ConstOperation getConstOperation() {
        return constOperation;
    }

    public void setConstOperation(ConstOperation constOperation) {
        this.constOperation = constOperation;
    }
}
