package ast.model.Var;

import ast.model.Function.Function;
import ast.model.Operation.MathOperation;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Constt extends Node {

    String CHAR;
    //or
    String NUMBER;
    //or
    MathOperation mathOperation;
    //or
    Function function;

    //or
    ConstOperation constOperation;
    //or
    String BRACOPEN;
    Constt constt;
    String BRACCLOSE;


    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Const:");
        if (CHAR != null) {
            printSpaces(spaceCount + 2);
            System.out.println(CHAR);
        } else if (NUMBER != null) {
            printSpaces(spaceCount + 2);
            System.out.println(NUMBER);
        } else if (mathOperation != null)
            mathOperation.accept(astVisitor, spaceCount + 2);
        else if (function != null)
            function.accept(astVisitor, spaceCount + 2);
        else if (constOperation != null) {
            constOperation.accept(astVisitor, spaceCount + 2);
        } else {
            //System.out.println(BRACOPEN);
            constt.accept(astVisitor, spaceCount);
            //System.out.println(BRACCLOSE);
        }
        printSpaces(spaceCount);
        System.out.println("Const End ......");
    }

    public String getCHAR() {
        return CHAR;
    }

    public void setCHAR(String CHAR) {
        this.CHAR = CHAR;
    }


    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public MathOperation getMathOperation() {
        return mathOperation;
    }

    public void setMathOperation(MathOperation mathOperation) {
        this.mathOperation = mathOperation;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public ConstOperation getConstOperation() {
        return constOperation;
    }

    public void setConstOperation(ConstOperation constOperation) {
        this.constOperation = constOperation;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public Constt getConstt() {
        return constt;
    }

    public void setConstt(Constt constt) {
        this.constt = constt;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }
}


