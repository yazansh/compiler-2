package ast.model.Var;

import ast.model.Function.Function;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Var extends Node {

    Simple_var simple_var;
    ArrayList<DotVar> dotVars = new ArrayList<>();
    //or
    Function function;
    //--//
    //or
    String BRACOPEN;
    Var var;
    String BRACCLOSE;
    //or
    String APOSTROPHE1;
    //--//
    ArrayList<String> COLUMN = new ArrayList<>();
    String APOSTROPHE2;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Var: ");
        if (simple_var != null) {
            simple_var.accept(astVisitor,spaceCount+2);
            for (int i = 0; i < dotVars.size(); i++) {
                dotVars.get(i).accept(astVisitor,spaceCount+2);
            }
        } else if (function != null) {
            function.accept(astVisitor,spaceCount+2);
            for (int i = 0; i < dotVars.size(); i++) {
                dotVars.get(i).accept(astVisitor,spaceCount+2);
            }
        } else if (BRACOPEN != null) {
            //System.out.println(BRACOPEN);
            var.accept(astVisitor,spaceCount);
            //System.out.println(BRACCLOSE);
        } else {
            //System.out.println(APOSTROPHE1);
            var.accept(astVisitor,spaceCount);
            //System.out.println(COLUMN);
            //System.out.println(APOSTROPHE2);
        }
        printSpaces(spaceCount);
        System.out.println("Var End .....");
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }

    public String getAPOSTROPHE1() {
        return APOSTROPHE1;
    }

    public void setAPOSTROPHE1(String APOSTROPHE1) {
        this.APOSTROPHE1 = APOSTROPHE1;
    }

    public String getAPOSTROPHE2() {
        return APOSTROPHE2;
    }

    public void setAPOSTROPHE2(String APOSTROPHE2) {
        this.APOSTROPHE2 = APOSTROPHE2;
    }

    public ArrayList<String> getCOLUMN() {
        return COLUMN;
    }

    public void setCOLUMN(ArrayList<String> COLUMN) {
        this.COLUMN = COLUMN;
    }

    public Simple_var getSimple_var() {
        return simple_var;
    }

    public void setSimple_var(Simple_var simple_var) {
        this.simple_var = simple_var;
    }

    public ArrayList<DotVar> getDotVars() {
        return dotVars;
    }

    public void setDotVars(ArrayList<DotVar> dotVars) {
        this.dotVars = dotVars;
    }
}
