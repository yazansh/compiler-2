package ast.model.Attribue;

import ast.model.Event.Event;
import ast.model.For.Cp_for;
import ast.model.Hide.Cp_hide;
import ast.model.If.Cp_if;
import ast.model.Show.Cp_show;
import ast.model.Switch.Cp_switch;
import ast.model.Switch.Cp_switchDefault;
import ast.model.Switch.Cp_switch_case;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class CpAttribute extends Node {

    Cp_show cp_show;
    Cp_for cp_for;
    Cp_if cp_if;
    Cp_hide cp_hide;
    Cp_switch cp_switch;
    Cp_switch_case cp_switch_case;
    Cp_switchDefault cp_switchDefault;
    Event event;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (cp_for != null)
            cp_for.accept(astVisitor,spaceCount);
        else if (cp_hide != null)
            cp_hide.accept(astVisitor,spaceCount);
        else if (cp_if != null)
            cp_if.accept(astVisitor,spaceCount);
        else if (cp_show != null)
            cp_show.accept(astVisitor,spaceCount);
        else if (cp_switch != null)
            cp_switch.accept(astVisitor,spaceCount);
        else if (cp_switch_case != null)
            cp_switch_case.accept(astVisitor,spaceCount);
        else if (cp_switchDefault != null)
            cp_switchDefault.accept(astVisitor,spaceCount);
        else if (event != null)
            event.accept(astVisitor,spaceCount);
    }

    public Cp_show getCp_show() {
        return cp_show;
    }

    public void setCp_show(Cp_show cp_show) {
        this.cp_show = cp_show;
    }

    public Cp_for getCp_for() {
        return cp_for;
    }

    public void setCp_for(Cp_for cp_for) {
        this.cp_for = cp_for;
    }

    public Cp_if getCp_if() {
        return cp_if;
    }

    public void setCp_if(Cp_if cp_if) {
        this.cp_if = cp_if;
    }

    public Cp_hide getCp_hide() {
        return cp_hide;
    }

    public void setCp_hide(Cp_hide cp_hide) {
        this.cp_hide = cp_hide;
    }

    public Cp_switch getCp_switch() {
        return cp_switch;
    }

    public void setCp_switch(Cp_switch cp_switch) {
        this.cp_switch = cp_switch;
    }

    public Cp_switch_case getCp_switch_case() {
        return cp_switch_case;
    }

    public void setCp_switch_case(Cp_switch_case cp_switch_case) {
        this.cp_switch_case = cp_switch_case;
    }

    public Cp_switchDefault getCp_switchDefault() {
        return cp_switchDefault;
    }

    public void setCp_switchDefault(Cp_switchDefault cp_switchDefault) {
        this.cp_switchDefault = cp_switchDefault;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }
}
