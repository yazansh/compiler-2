package ast.model.Attribue;

import ast.model.HtmlS.HtmlAttribute;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class ElementAttribute extends Node {

    private HtmlAttribute htmlAttribute;
    //or
    private CpAttribute cpAttribute;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        if (htmlAttribute != null)
            htmlAttribute.accept(astVisitor,spaceCount);
        else
            cpAttribute.accept(astVisitor,spaceCount);
    }

    public HtmlAttribute getHtmlAttribute() {
        return htmlAttribute;
    }

    public void setHtmlAttribute(HtmlAttribute htmlAttribute) {
        this.htmlAttribute = htmlAttribute;
    }

    public CpAttribute getCpAttribute() {
        return cpAttribute;
    }

    public void setCpAttribute(CpAttribute cpAttribute) {
        this.cpAttribute = cpAttribute;
    }


}
