package ast.model.Attribue;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class AttributeValue extends Node {

    String TAG_EQUALS;
    String ATTVALUE_VALUE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.print(TAG_EQUALS);
        printSpaces(spaceCount);
        System.out.println(ATTVALUE_VALUE);
    }

    public String getTAG_EQUALS() {
        return TAG_EQUALS;
    }

    public void setTAG_EQUALS(String TAG_EQUALS) {
        this.TAG_EQUALS = TAG_EQUALS;
    }

    public String getATTVALUE_VALUE() {
        return ATTVALUE_VALUE;
    }

    public void setATTVALUE_VALUE(String ATTVALUE_VALUE) {
        this.ATTVALUE_VALUE = ATTVALUE_VALUE;
    }
}
