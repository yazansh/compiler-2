package ast.model.Expression;

import ast.model.Array.Array;
import ast.model.If.One_line_condition;
import ast.model.Object.Objectt;
import ast.model.Operation.logic;
import ast.model.Var.Constt;
import ast.model.Var.Var;
import ast.model.boolen;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Expression extends Node {

    Var var;
    //or
    Constt constt;
    //or
    Array array;
    //or
    logic logic;
    //or
    One_line_exp one_line_exp;
    //or
    Objectt objectt;
    //or
    boolen boolen;
    //or
    One_line_condition one_line_condition;


    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (var != null)
            var.accept(astVisitor,spaceCount);
        else if (constt != null)
            constt.accept(astVisitor,spaceCount);
        else if (array != null)
            array.accept(astVisitor,spaceCount);
        else if (logic != null)
            logic.accept(astVisitor,spaceCount);
        else if (one_line_exp != null)
            one_line_exp.accept(astVisitor,spaceCount);
        else if (objectt != null)
            objectt.accept(astVisitor,spaceCount);
        else if (boolen != null)
            boolen.accept(astVisitor,spaceCount);
        else
            one_line_condition.accept(astVisitor,spaceCount);
    }

    public One_line_condition getOne_line_condition() {
        return one_line_condition;
    }

    public void setOne_line_condition(One_line_condition one_line_condition) {
        this.one_line_condition = one_line_condition;
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public Constt getConstt() {
        return constt;
    }

    public void setConstt(Constt constt) {
        this.constt = constt;
    }

    public Array getArray() {
        return array;
    }

    public void setArray(Array array) {
        this.array = array;
    }

    public ast.model.Operation.logic getLogic() {
        return logic;
    }

    public void setLogic(logic logic) {
        this.logic = logic;
    }

    public ast.model.boolen getBoolen() {
        return boolen;
    }

    public void setBoolen(ast.model.boolen boolen) {
        this.boolen = boolen;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public Objectt getObjectt() {
        return objectt;
    }

    public void setObjectt(Objectt objectt) {
        this.objectt = objectt;
    }
}
