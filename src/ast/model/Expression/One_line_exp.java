package ast.model.Expression;

import ast.model.Function.Function;
import ast.model.Operation.logic;
import ast.model.Var.Var;
import ast.model.boolen;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class One_line_exp extends Node {

    Var var;
    //or
    String TRUE;
    //or
    String FALSE;
    //or
    logic logic;
    //or
    Function function;
    //or
    boolen boolen;

    String QMARK;
    Expression expression;
    String COLUMN;
    Expression expressionSecond;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("One Line Expression:");
        if (var != null)
            var.accept(astVisitor, spaceCount + 2);
        else if (TRUE != null)
            System.out.println(TRUE);
        else if (FALSE != null)
            System.out.println(FALSE);
        else if (logic != null)
            logic.accept(astVisitor, spaceCount+2);
        else if (function != null)
            function.accept(astVisitor, spaceCount+2);
        else if (boolen != null)
            boolen.accept(astVisitor, spaceCount+2);

        printSpaces(spaceCount);
        System.out.println("First Result:");
        expression.accept(astVisitor, spaceCount+2);
        System.out.println("Second Result:");
        expressionSecond.accept(astVisitor, spaceCount+2);

        printSpaces(spaceCount);
        System.out.println("One Line Expression End .....");
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public String getTRUE() {
        return TRUE;
    }

    public void setTRUE(String TRUE) {
        this.TRUE = TRUE;
    }

    public String getFALSE() {
        return FALSE;
    }

    public void setFALSE(String FALSE) {
        this.FALSE = FALSE;
    }


    public ast.model.Operation.logic getLogic() {
        return logic;
    }

    public void setLogic(ast.model.Operation.logic logic) {
        this.logic = logic;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public ast.model.boolen getBoolen() {
        return boolen;
    }

    public void setBoolen(ast.model.boolen boolen) {
        this.boolen = boolen;
    }

    public String getQMARK() {
        return QMARK;
    }

    public void setQMARK(String QMARK) {
        this.QMARK = QMARK;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public String getCOLUMN() {
        return COLUMN;
    }

    public void setCOLUMN(String COLUMN) {
        this.COLUMN = COLUMN;
    }

    public Expression getExpressionSecond() {
        return expressionSecond;
    }

    public void setExpressionSecond(Expression expressionSecond) {
        this.expressionSecond = expressionSecond;
    }
}
