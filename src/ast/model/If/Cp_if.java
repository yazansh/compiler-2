package ast.model.If;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Cp_if extends Node {

    String CP_IF;
    String EQUALL;
    String QUOTETION_OPEN;
    Expression expression;
    String QUOTETION_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("CP_IF:");
        //System.out.print(EQUALL);
        //System.out.println(QUOTETION_OPEN);
        expression.accept(astVisitor, spaceCount + 2);
        //System.out.println(QUOTETION_CLOSE);
        printSpaces(spaceCount);
        System.out.println("CP_IF End .....");
    }

    public String getCP_IF() {
        return CP_IF;
    }

    public void setCP_IF(String CP_IF) {
        this.CP_IF = CP_IF;
    }

    public String getEQUALL() {
        return EQUALL;
    }

    public void setEQUALL(String EQUALL) {
        this.EQUALL = EQUALL;
    }

    public String getQUOTETION_OPEN() {
        return QUOTETION_OPEN;
    }

    public void setQUOTETION_OPEN(String QUOTETION_OPEN) {
        this.QUOTETION_OPEN = QUOTETION_OPEN;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public String getQUOTETION_CLOSE() {
        return QUOTETION_CLOSE;
    }

    public void setQUOTETION_CLOSE(String QUOTETION_CLOSE) {
        this.QUOTETION_CLOSE = QUOTETION_CLOSE;
    }
}
