package ast.model.If;

import ast.model.Expression.One_line_exp;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class One_line_condition extends Node {

    String BRACOPEN;
    One_line_exp one_line_exp;
    String BRACCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(BRACOPEN);
        one_line_exp.accept(astVisitor, spaceCount);
        //System.out.println(BRACCLOSE);
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }
}
