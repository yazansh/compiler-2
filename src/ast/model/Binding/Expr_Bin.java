package ast.model.Binding;

import ast.model.Var.Var;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class Expr_Bin  extends Node {

    Var var;
    String COLUMN_B;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        var.accept(astVisitor,spaceCount);
        //System.out.print(COLUMN_B);
    }

    public Var getVar() {
        return var;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public String getCOLUMN_B() {
        return COLUMN_B;
    }

    public void setCOLUMN_B(String COLUMN_B) {
        this.COLUMN_B = COLUMN_B;
    }
}
