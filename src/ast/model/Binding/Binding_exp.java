package ast.model.Binding;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Binding_exp extends Node {

    Expr_Bin expr_bin;
    //or
    Expression expression;

    ArrayList<BindingPipe> bindingPipe = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor , int spaceCount) {
        astVisitor.visit(this);
        if (expr_bin != null)
            expr_bin.accept(astVisitor,spaceCount);
        else
            expression.accept(astVisitor,spaceCount);
        for (int i = 0; i < bindingPipe.size(); i++) {
            bindingPipe.get(i).accept(astVisitor,spaceCount);
        }
    }

    public Expr_Bin getExpr_bin() {
        return expr_bin;
    }

    public void setExpr_bin(Expr_Bin expr_bin) {
        this.expr_bin = expr_bin;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public ArrayList<BindingPipe> getBindingPipe() {
        return bindingPipe;
    }

    public void setBindingPipe(ArrayList<BindingPipe> bindingPipe) {
        this.bindingPipe = bindingPipe;
    }
}
