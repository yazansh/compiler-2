package ast.model.Binding;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Binding extends Node {

    String BINDING_OPEN;
    ArrayList<Binding_exp> binding_exp = new ArrayList<>();
    ArrayList<Expression> expression = new ArrayList<>();
    String BINDING_CLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.print(BINDING_OPEN);
        printSpaces(spaceCount);
        System.out.println("Binding:");
        if (!binding_exp.isEmpty()) {
            for (int i = 0; i < binding_exp.size(); i++) {
                binding_exp.get(i).accept(astVisitor, spaceCount+2);
            }
        } else
            for (int i = 0; i < expression.size(); i++) {
                expression.get(i).accept(astVisitor, spaceCount+2);
            }
        printSpaces(spaceCount);
        System.out.println("Binding End.....");
        //System.out.println(BINDING_CLOSE);
    }

    public ArrayList<Binding_exp> getBinding_exp() {
        return binding_exp;
    }

    public void setBinding_exp(ArrayList<Binding_exp> binding_exp) {
        this.binding_exp = binding_exp;
    }

    public ArrayList<Expression> getExpression() {
        return expression;
    }

    public void setExpression(ArrayList<Expression> expression) {
        this.expression = expression;
    }

    public String getBINDING_OPEN() {
        return BINDING_OPEN;
    }

    public void setBINDING_OPEN(String BINDING_OPEN) {
        this.BINDING_OPEN = BINDING_OPEN;
    }

    public String getBINDING_CLOSE() {
        return BINDING_CLOSE;
    }

    public void setBINDING_CLOSE(String BINDING_CLOSE) {
        this.BINDING_CLOSE = BINDING_CLOSE;
    }
}
