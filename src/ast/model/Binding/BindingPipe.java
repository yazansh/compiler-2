package ast.model.Binding;

import ast.model.Expression.Expression;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class BindingPipe extends Node {

    String PIPE;
    Expr_Bin expr_bin;
    //or
    Expression expression;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.print("PIPE ..");
        if (expr_bin != null)
            expr_bin.accept(astVisitor,spaceCount);
        else
            expression.accept(astVisitor,spaceCount);
    }

    public String getPIPE() {
        return PIPE;
    }

    public void setPIPE(String PIPE) {
        this.PIPE = PIPE;
    }

    public Expr_Bin getExpr_bin() {
        return expr_bin;
    }

    public void setExpr_bin(Expr_Bin expr_bin) {
        this.expr_bin = expr_bin;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
