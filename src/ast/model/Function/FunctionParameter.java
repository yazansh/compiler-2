package ast.model.Function;

import ast.model.Expression.Expression;
import ast.model.Expression.One_line_exp;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class FunctionParameter extends Node {

    Expression expression;
    //or
    One_line_exp one_line_exp;
    ArrayList<AnotherFunctionParameter> anotherFunctionParameter = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        if (expression != null)
            expression.accept(astVisitor, spaceCount);
        else
            one_line_exp.accept(astVisitor, spaceCount);
        for (int i = 0; i < anotherFunctionParameter.size(); i++) {
            anotherFunctionParameter.get(i).accept(astVisitor, spaceCount);
        }
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }

    public ArrayList<AnotherFunctionParameter> getAnotherFunctionParameter() {
        return anotherFunctionParameter;
    }

    public void setAnotherFunctionParameter(ArrayList<AnotherFunctionParameter> anotherFunctionParameter) {
        this.anotherFunctionParameter = anotherFunctionParameter;
    }
}
