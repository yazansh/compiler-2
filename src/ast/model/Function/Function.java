package ast.model.Function;

import ast.model.Array.Array_element;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Function extends Node {


    //this:
    String VARNAME;
    //or this:
    Array_element array_element;

    //function_expr+
    ArrayList<Function_expr> function_expr = new ArrayList<>();

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        printSpaces(spaceCount);
        System.out.println("Function:");
        if (VARNAME != null) {
            printSpaces(spaceCount + 2);
            System.out.println(VARNAME);
        } else
            array_element.accept(astVisitor, spaceCount + 2);
        for (int i = 0; i < function_expr.size(); i++) {
            function_expr.get(i).accept(astVisitor, spaceCount + 2);
        }
        printSpaces(spaceCount);
        System.out.println("Function End .....");
    }

    public String getVARNAME() {
        return VARNAME;
    }

    public void setVARNAME(String VARNAME) {
        this.VARNAME = VARNAME;
    }

    public Array_element getArray_element() {
        return array_element;
    }

    public void setArray_element(Array_element array_element) {
        this.array_element = array_element;
    }

    public ArrayList<Function_expr> getFunction_expr() {
        return function_expr;
    }

    public void setFunction_expr(ArrayList<Function_expr> function_expr) {
        this.function_expr = function_expr;
    }
}
