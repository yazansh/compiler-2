package ast.model.Function;

import ast.nodes.Node;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class Function_expr extends Node {

    String BRACOPEN;
    ArrayList<FunctionParameter> functionParameter = new ArrayList<>();
    String BRACCLOSE;

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(BRACOPEN);
        for (int i = 0; i < functionParameter.size(); i++) {
            functionParameter.get(i).accept(astVisitor,spaceCount);
        }
        //System.out.println(BRACCLOSE);
    }

    public String getBRACOPEN() {
        return BRACOPEN;
    }

    public void setBRACOPEN(String BRACOPEN) {
        this.BRACOPEN = BRACOPEN;
    }

    public ArrayList<FunctionParameter> getFunctionParameter() {
        return functionParameter;
    }

    public void setFunctionParameter(ArrayList<FunctionParameter> functionParameter) {
        this.functionParameter = functionParameter;
    }

    public String getBRACCLOSE() {
        return BRACCLOSE;
    }

    public void setBRACCLOSE(String BRACCLOSE) {
        this.BRACCLOSE = BRACCLOSE;
    }
}
