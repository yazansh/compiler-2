package ast.model.Function;

import ast.model.Expression.Expression;
import ast.model.Expression.One_line_exp;
import ast.nodes.Node;
import ast.visitor.ASTVisitor;

public class AnotherFunctionParameter extends Node {

    String COMMA;
    Expression expression;
    //or
    One_line_exp one_line_exp;

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        //System.out.println(COMMA);
        if (expression != null)
            expression.accept(astVisitor,spaceCount);
        else
            one_line_exp.accept(astVisitor,spaceCount);
    }

    public String getCOMMA() {
        return COMMA;
    }

    public void setCOMMA(String COMMA) {
        this.COMMA = COMMA;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public One_line_exp getOne_line_exp() {
        return one_line_exp;
    }

    public void setOne_line_exp(One_line_exp one_line_exp) {
        this.one_line_exp = one_line_exp;
    }
}
