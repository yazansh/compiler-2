package ast.nodes;

import ast.model.HtmlS.HtmlElement;
import ast.model.HtmlS.HtmlMisc;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;

public class HTMLElements extends Node {

    private ArrayList<HtmlMisc> htmlMiscs = new ArrayList<>();

    private HtmlElement htmlElement;

    public HtmlElement getHtmlElement() {
        return htmlElement;
    }

    public void setHtmlElement(HtmlElement htmlElement) {
        this.htmlElement = htmlElement;
    }

    public ArrayList<HtmlMisc> getHtmlMiscs() {
        return htmlMiscs;
    }

    public void setHtmlMiscs(ArrayList<HtmlMisc> htmlMiscs) {
        this.htmlMiscs = htmlMiscs;
    }

    @Override
    public void accept(ASTVisitor astVisitor,int spaceCount) {
        astVisitor.visit(this);
        for (int i = 0; i < htmlMiscs.size(); i++) {
            htmlMiscs.get(i).accept(astVisitor,spaceCount);
        }
        htmlElement.accept(astVisitor,spaceCount);
    }
}
