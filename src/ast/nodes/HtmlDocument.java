package ast.nodes;

import ast.model.HtmlS.ScriptletOrSeaWs;
import ast.visitor.ASTVisitor;

import java.util.ArrayList;
import java.util.List;

public class HtmlDocument extends Node {
    private List<HTMLElements> elements = new ArrayList<>();
    private List<ScriptletOrSeaWs> scriptletOrSeaWs = new ArrayList<>();
    private String DTD;//DTD?

    @Override
    public void accept(ASTVisitor astVisitor, int spaceCount) {
        astVisitor.visit(this);
        for (int i = 0; i < elements.size(); i++) {
            elements.get(i).accept(astVisitor, spaceCount);
        }
        for (int i = 0; i < scriptletOrSeaWs.size(); i++) {
            scriptletOrSeaWs.get(i).accept(astVisitor, spaceCount);
        }
    }

    public String getDTD() {
        return DTD;
    }

    public void setDTD(String DTD) {
        this.DTD = DTD;
    }

    public List<ScriptletOrSeaWs> getScriptletOrSeaWs() {
        return scriptletOrSeaWs;
    }

    public void setScriptletOrSeaWs(List<ScriptletOrSeaWs> scriptletOrSeaWs) {
        this.scriptletOrSeaWs = scriptletOrSeaWs;
    }

    public void setElements(List<HTMLElements> elements) {
        this.elements = elements;
    }

    public List<HTMLElements> getElements() {
        return elements;
    }
}
