package tareq;

import SymbolTable.*;
import ast.nodes.HtmlDocument;
import ast.visitor.BaseASTVisitor;
import ast.visitor.BaseVisitor;

import gen.HTMLLexer;
import gen.HTMLParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;



import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {

    public static  SymbolTable symbolTable =new SymbolTable();
    public static TagTable tagTable = new TagTable();

    public static void main(String[] args) {
        try {
            String source = "samples//samples.txt";
            CharStream cs = fromFileName(source);
            HTMLLexer lexer = new HTMLLexer(cs);
            CommonTokenStream token  = new CommonTokenStream(lexer);
            HTMLParser parser = new HTMLParser(token);
            ParseTree tree = parser.htmlDocument();
            HtmlDocument doc = (HtmlDocument) new BaseVisitor().visit(tree);
            doc.accept(new BaseASTVisitor(),0);
            System.out.println("The End!");
            doc.getElements();

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < symbolTable.getScopeStack().size() ; i++) {
            System.out.println(symbolTable.getScopeStack().get(i));
        }
        System.out.println("Number of Scopes in the program : " + Scope.getContID());
       // System.out.println(symbolTable.getScopes().get(1).getSymbolMap().get("as"));
        for (int i = 0; i <symbolTable.getScopes().size() ; i++) {

            for (String name : symbolTable.getScopes().get(i).getSymbolMap().keySet()) {
                String key = name;
                System.out.println(name);
                Symbol value = symbolTable.getScopes().get(i).getSymbolMap().get(name);
                value.print();
                System.out.println();
            }
        }

        //System.out.println(symbolTable.getScopes().get(0));
        System.out.println();
        System.out.println();
        tagTable.print();

        System.out.println(symbolTable.getSymbolScope("a"));
    }
}
