
parser grammar HTMLParser;

options { tokenVocab=HTMLLexer; }

htmlDocument
    : scriptletOrSeaWs* XML? scriptletOrSeaWs* DTD? scriptletOrSeaWs* htmlElements*
    ;

scriptletOrSeaWs
    : SCRIPTLET
    | SEA_WS
    ;

htmlElements
    : htmlMisc* htmlElement htmlMisc*
    ;

elementAttribute
: htmlAttribute|cpAttribute;


containerHtmlContent
    :htmlContent TAG_OPEN TAG_SLASH TAG_NAME TAG_CLOSE;

htmlElement
    : TAG_OPEN TAG_NAME elementAttribute*
      (TAG_CLOSE containerHtmlContent? | TAG_SLASH_CLOSE)
    | SCRIPTLET
    | script
    | style
    ;

htmlStructure///
        :(htmlElement | CDATA | htmlComment |binding) htmlChardata?
    ;

htmlContent///
    : htmlChardata? (htmlStructure)*
    ;


attributeValue///
    :TAG_EQUALS ATTVALUE_VALUE
    ;

htmlAttribute
    : TAG_NAME (attributeValue)?///
    ;
/* CP Attribute */
    cpAttribute
    :cp_show
    |cp_for
    |cp_if
    |cp_hide
    |cp_switch
    |cp_switch_case
    |cp_switchDefault
    |event
    ;

cp_show: CP_SHOW EQUALL QUOTETION_OPEN expression QUOTETION_CLOSE ;
cp_for: CP_FOR EQUALL QUOTETION_OPEN (for_ExprFirst|for_ExprSecond|expression) QUOTETION_CLOSE;
cp_if: CP_IF EQUALL QUOTETION_OPEN expression QUOTETION_CLOSE;
cp_hide:CP_HIDE EQUALL QUOTETION_OPEN expression QUOTETION_CLOSE;
cp_switch:CP_SWITCH EQUALL QUOTETION_OPEN (var|one_line_exp) QUOTETION_CLOSE;////
cp_switch_case: CP_SWITCH_CASE EQUALL QUOTETION_OPEN (constt|var|one_line_exp|logic) QUOTETION_CLOSE;
cp_switchDefault: CP_SWITCHDEFAULT;
event: EVENT_NAME EQUALL QUOTETION_OPEN function QUOTETION_CLOSE;///
/* CP Attribute End */

htmlChardata
    : HTML_TEXT
    | SEA_WS
    ;

htmlMisc
    : htmlComment
    | SEA_WS
    ;

htmlComment
    : HTML_COMMENT
    | HTML_CONDITIONAL_COMMENT
    ;

expression
    //call function , one line condition, variable name (even inside an object) , array element, boolean expression
    :(var|constt|array|logic|one_line_exp|objectt|boolen|one_line_condition)
    ;

/* Binding */
//binding: BINDING_OPEN (expression_bin|function_bin|operation_B)* BINDING_CLOSE;
binding
    :BINDING_OPEN (binding_exp|expression)* BINDING_CLOSE
    ;

binding_exp
    : (expr_Bin|expression) bindingPipe*
    ;

bindingPipe
    :PIPE (expr_Bin|expression)
    ;

expr_Bin: var COLUMN_B ;
/* Binding End */

/* For Expression */
for_ExprFirst
    :beforIN IN afterIN afterSemiColonn?
    ;

for_ExprSecond
    :beforIN COMMA beforIN  IN afterIN
    ;

beforIN:
    (var|one_line_exp)
    ;

afterIN:
    (var|array|one_line_exp)
    ;

afterSemiColonn:
    (SEMICOLONN (var|one_line_exp) EQUAL (INDEX|var|NUMBER|one_line_exp))
    ;
/* For Expression End */

constt
    :CHAR
//    |STRING
    |NUMBER
    |mathOperation
    |function
    |constOperation
    |BRACOPEN constt BRACCLOSE
    ;

constOperation
    :(CHAR|var|function) (constSide)*
    ;

constSide
    :(PLUS|MINUS) constOperation
    ;

/*Variable*/
simple_var
         :VARNAME
         |array_element
         |one_line_condition
         |BRACOPEN simple_var BRACCLOSE
         ;

dotVar
    :DOT var
    ;

var
    :simple_var (dotVar)*
    |function (dotVar)+
    |BRACOPEN var BRACCLOSE
    |APOSTROPHE var COLUMN* APOSTROPHE // what is this
    ;
/* Variable End */

/* boolean Operation ... */ // 4>(6)
boolen
    :booleanSide BOOLEAN booleanSide
    |TRUE
    |FALSE
    |function
    |BRACOPEN boolen BRACCLOSE
    |NOT (boolen|var|function)
    ;

booleanSide
    :(var|NUMBER|mathOperation|function)
    ;

logic
    :(boolen|var) logicSide+
    |NOT? (BRACOPEN logic BRACCLOSE)
    ;

logicSide
    :(LOGIC (boolen|var))
    ;
/* Logic Operation End ... */

/* Math Operation*/ // 5+5+3*2+(2*6+2)*2+function+ArrayElement

mathOperation
    :(operationSide) operationSideWithPlusMinus*
    |operationElement (PLUSPLUS|MINUSMINUS)
    ;

operationSideWithPlusMinus
    :(PLUS|MINUS) (operationSide)
    ;

operationSide //(3+2)*5
    :(operationElement |(BRACOPEN mathOperation BRACCLOSE)) operationSideWithDivMultRemain*
    |BRACOPEN operationSide BRACCLOSE
    ;

operationSideWithDivMultRemain: ((DIV|MULT|REMAIN) operationSide) ;

operationElement
    :NUMBER|var|function
    ;
/* End Math Operation */

/*Object*/
anotherObj
    :COMMA expr_object
    ;

objectt
    :CURLYOPEN expr_object (anotherObj)* CURLYCLOSE
    ;

expr_object: VARNAME COLUMN (expression);
/* Object End */

/* One Line Condition */
one_line_condition  : (BRACOPEN one_line_exp BRACCLOSE) ;

one_line_exp:(var|TRUE|FALSE|logic|function|boolen) QMARK expression COLUMN expression;
/* One Line Condition End */

/* Array ... */
array
    : ARRAYOPEN (expression) (anotherArrayElement)* ARRAYCLOSE
    |BRACOPEN array BRACCLOSE
    ;

anotherArrayElement
    :COMMA (expression)
    ;

arrayElementExp
    :function_expr* (arrayIndex)+
    ;

arrayIndex
    :ARRAYOPEN (NUMBER|var|function) ARRAYCLOSE
    ;

array_element
    :VARNAME (arrayElementExp)+
    ;
/* Array End ... */

/* Function ... */
function
    : (VARNAME|array_element) function_expr+
    ;

function_expr
    :(BRACOPEN (functionParameter)* BRACCLOSE)
    ;

functionParameter
    : (expression|one_line_exp) (anotherFunctionParameter)*
    ;

anotherFunctionParameter
    :COMMA (expression|one_line_exp)
    ;
/* Function End ... */

script
    : SCRIPT_OPEN (SCRIPT_BODY | SCRIPT_SHORT_BODY)
    ;

style
    : STYLE_OPEN (STYLE_BODY | STYLE_SHORT_BODY)
    ;
